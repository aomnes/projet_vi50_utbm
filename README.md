## Projet : VR game VI50/TX52

[Vidéo de présentation](https://youtu.be/RhqYAZucDFI)

Projet UTBM dans le cadre d'un cours sur la réalité virtuelle en utilisant Unity3D.
Le but de ce projet est de mettre en pratique nos connaissances en Réalité Virtuelle afin de créer une application.

Il est nécessaire de posséder l'Oculus Rift ainsi qu'un PC compatible VR avec l'utilitaire Oculus installé. 

# Boutons du Jeu:

## Touches du clavier:
- Q : Quitter l'application
- M + N : Reset Casque orientation/position
- G : "Abandonner" un mini-jeu une fois dans la scène du mini-jeu
- A : Commencer un mini-jeu une fois dans la scène du mini-jeu
- I + O + P : Charger la scene principale avec tous les mini-jeux notés en "fini"

## Oculus Touch
- B : Commencer un mini-jeu une fois dans la scene du mini-jeu
- A et X ou B et Y : "Abandonner" un mini-jeu une fois dans la scène du mini-jeu
