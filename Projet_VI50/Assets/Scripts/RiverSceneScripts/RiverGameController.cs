﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class RiverGameController : MonoBehaviour {
    
    #region SerializeField
    
    [FormerlySerializedAs("imageDisplay")] [SerializeField] private ImageDisplay _imageDisplay;
    [FormerlySerializedAs("cubeSpawner")] [SerializeField] private CubeSpawner _cubeSpawner;
    [FormerlySerializedAs("boxSpawner")] [SerializeField] private BoxSpawner _boxSpawner;
    [FormerlySerializedAs("scoreDisplay")] [SerializeField] private GameObject _scoreDisplay;
    [FormerlySerializedAs("successSound")] [SerializeField] private AudioSource _successSound;
    [FormerlySerializedAs("textCanvas")] [SerializeField] private Canvas _textCanvas;
    [FormerlySerializedAs("instructionsText")] [SerializeField] private Text _instructionsText;
    [FormerlySerializedAs("calculusText")] [SerializeField] private Text _calculusText;
    
    #endregion

    #region Private fields

    private GameType _gameType;
    private SchoolLevels _level;
    private string[] _words = new string[100];
    private List<Calculation> _calculations = new List<Calculation>();
    private int _turnIndex = 0;
    private GameTurnHandler _turnHandler;
    private const string ScoreString = "SCORE: ";
    private bool _startGame;
    private int currentScore = 0;
    private const int maxScore = 100;

    #endregion

    #region UnityFunctions

    void Start()
    {
        name = Constants.riverGameControllerName;
        SetDataGameInformation();
        ShowInstructions();
        TextMesh scoreText = _scoreDisplay.GetComponent<TextMesh>();
        scoreText.text = "0 / " + maxScore.ToString();
        switch (_gameType)
        {
            case GameType.French:
                SetupFrenchGameplay();
                break;
            case GameType.Math:
                SetupMathGameplay();
                break;
        }
    }

    void Update()
    { 
        if (!_startGame && ActionButtonTouch.IsActiveButtonStartGame())
        {
            switch (_gameType)
            {
                case GameType.French:
                    _textCanvas.gameObject.SetActive(false);
                    StartFrenchGame();
                    _startGame = true;
                    break;
                case GameType.Math:
                    _instructionsText.gameObject.SetActive(false);
                    StartMathGame();
                    _startGame = true;
                    break;
            }
        }

        if (_startGame && OVRInput.Get(OVRInput.RawButton.A))
        {
            SetDataGameProgress();
            ActionButtonTouch.GiveUp();
        }
    }

    #endregion

    #region Public functions

    public void OnBoxFilled(string guessedLetter, string boxLetter)
    {
        _turnHandler.ElementGuessed(guessedLetter, boxLetter);
        if (_turnHandler.IsGuessCorrect())
        {
            _successSound.Play();
            NextFrenchTurn();
        }
    }

    public void OnBoxFilled(float number)
    {
        Calculation c = _calculations[_turnIndex];
        _turnHandler.UpdateMathScore(_calculations[_turnIndex], number);
        if (_turnHandler.IsGuessCorrect(c, number))
        {
            _successSound.Play();
            NextMathTurn();
        }
    }

    public void OnBoxEmptied(string boxLetter)
    {
        _turnHandler.ElementRemoved(boxLetter);
    }

    #endregion

    #region Private methods

    private void SetDataGameInformation()
    {
        if (DataGame.Instance == null)
        {
            _gameType = GameType.Math;
            _level = SchoolLevels.CP;
            return;
        }
        
        MiniJeuScriptableObject game = DataGame.Instance.CurrentGame;
        _gameType = game.GameType;
        _level = DataGame.Instance.DataPlayer.NameClasseStudent;
    }

    private void SetupFrenchGameplay()
    {
        _words = FrenchGameDataHelper.GetWords(_level);
        UpdateScoreDisplay();
    }

    private void SetupMathGameplay()
    {
        _calculusText.gameObject.SetActive(true);
        _calculations = MathGameDataHelper.GetCalculations(_level);
        UpdateScoreDisplay();
    }

    private void StartFrenchGame()
    {
        string word = _words[0];
        _turnHandler = new GameTurnHandler(word.ToUpper().ToCharArray(), maxScore);
        _boxSpawner.SetShouldSpawnBoxes(true, word);
        _cubeSpawner.SetShouldSpawnCubes(true, word.ToCharArray(), FrenchGameDataHelper.GetRandomLetters(_level));
        SetImageToFind(word);
    }

    private void StartMathGame()
    {
        Calculation c = _calculations[0];
        _turnHandler = new GameTurnHandler(c, maxScore);
        _boxSpawner.SetShouldSpawnBoxes(true, c.GetElementToGuess);
        _cubeSpawner.SetShouldSpawnCubes(true, c.GetElementToGuess, MathGameDataHelper.GetRandomNumbers(_level));
        SetCalculationToFind(c);
    }
    
    private void SetImageToFind(string imageName)
    {
        _imageDisplay.SetImage(imageName);
    }

    private void SetCalculationToFind(Calculation c)
    {
        _calculusText.text = c.ToString();
    }

    private void NextFrenchTurn()
    {
        _turnIndex++;
        UpdateScoreDisplay();
        if ((_turnIndex+1) >= _words.Length)
        {
            StartCoroutine(EndGame());
            return;
        }

        string word = _words[_turnIndex];
        _turnHandler = new GameTurnHandler(word.ToUpper().ToCharArray(), maxScore);
        _boxSpawner.RenewBoxes(word);
        _cubeSpawner.RenewCubes(word.ToCharArray(), FrenchGameDataHelper.GetRandomLetters(_level));
        SetImageToFind(word);
    }

    private void NextMathTurn()
    {
        _turnIndex++;
        UpdateScoreDisplay();
        if (_turnIndex+1 >= _calculations.Count)
        {
            StartCoroutine(EndGame());
            return;
        }

        Calculation c = _calculations[_turnIndex];
        _turnHandler = new GameTurnHandler(c, maxScore);
        _boxSpawner.RenewBoxes(c.GetElementToGuess);
        _cubeSpawner.RenewCubes(c.GetElementToGuess, MathGameDataHelper.GetRandomNumbers(_level));
        SetCalculationToFind(c);
    }

    private IEnumerator EndGame()
    {
        _textCanvas.enabled = false;
        SetImageToFind("bravo");
        yield return new WaitForSeconds(2f);
        ExitGame();
    }

    private void UpdateScoreDisplay()
    {
        TextMesh scoreText = _scoreDisplay.GetComponent<TextMesh>();
        if (_turnHandler == null)
        {
            scoreText.text = ScoreString + currentScore + "/" + maxScore;
            return;
        }
        
        switch (_gameType)
        {
            case GameType.French:
                currentScore += (_turnHandler.GetTurnScore() / _words.Length);
                scoreText.text = ScoreString + currentScore + "/" + maxScore;
                break;
            case GameType.Math:
                currentScore += (_turnHandler.GetTurnScore() / _calculations.Count);
                scoreText.text = ScoreString + currentScore + "/" + maxScore;
                break;
        }
    }

    private void ShowInstructions()
    {
        _calculusText.gameObject.SetActive(false);
            
        switch (_gameType)
        {
            case GameType.French:
                _instructionsText.text = Instructions.frenchGameInstructions;
                break;
            case GameType.Math:
                _instructionsText.text = Instructions.mathGameInstructions;
                break;
            default:
                throw new Exception("Game type not supported for mini game river.");
        }
    }

    private void ExitGame()
    {
        SetDataGameProgress();
        LevelManager.LoadMain();
    }

    private void SetDataGameProgress()
    {
        if (DataGame.Instance == null)
        {
            Debug.Log("DataGame is not set. Cannot save progress.");
            return;
        }
        
        DataGame.Instance.CurrentGame.Score = currentScore;
        switch (_gameType)
        {
            case GameType.French:
                DataGame.Instance.CurrentGame.IsDone = (_turnIndex+1) == _words.Length;
                break;
            case GameType.Math:
                DataGame.Instance.CurrentGame.IsDone = (_turnIndex+1) == _calculations.Count;
                break;
        }
    }

    #endregion
}
