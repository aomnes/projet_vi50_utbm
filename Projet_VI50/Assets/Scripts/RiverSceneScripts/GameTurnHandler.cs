﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTurnHandler {

    private char[] _elementsToFind;
    private char[] _elementsGuessed;
    private GameType _gameType;
    private Calculation _calculationToFind;
    private int _score;
    private int _maxScore;
    
    public GameTurnHandler(char[] elements, int maxScore)
    {
        _elementsToFind = elements;
        _elementsGuessed = new char[elements.Length];
        _gameType = GameType.French;
        _maxScore = maxScore;
        _score = 0;
    }
    
    public GameTurnHandler(Calculation c, int maxScore)
    {
        _calculationToFind = c;
        _gameType = GameType.Math;
        _maxScore = maxScore;
        _score = 0;
    }

    public void ElementGuessed(string element, string boxLetter)
    {
        int boxIndex = GetBoxIndex(boxLetter);
        if (boxIndex < 0 || boxIndex > _elementsToFind.Length)
        {
            return;
        }

        if (element.Equals(boxLetter))
        {
            _score += (100 / _elementsToFind.Length);
        }
        else
        {
            _score -= (100 / _elementsToFind.Length) / (1/3);
        }

        _elementsGuessed[boxIndex] = element.ToCharArray()[0];
    }

    public void UpdateMathScore(Calculation c, float number)
    {
        _score += (c.GetElementToGuess == number) ? 100 : -(100 / 3);
    } 

    public bool IsGuessCorrect(Calculation c, float number)
    {
        return c.GetElementToGuess == number;
    }

    public void ElementRemoved(string boxLetter)
    {
        int boxIndex = GetBoxIndex(boxLetter);
        if (boxIndex < 0)
        {
            return;
        }

        _elementsGuessed[boxIndex] = new char();
    }

    public int GetTurnScore()
    {
        return _score > 0 ? _score : 0;
    }

    public bool IsGuessCorrect()
    {
        for (int i = 0; i < _elementsToFind.Length; i++)
        {
            if (_elementsToFind[i] != _elementsGuessed[i])
            {
                return false;
            }
        }

        return true;
    }

    private int GetBoxIndex(string letter)
    {
        for (int i = 0; i < _elementsToFind.Length; i++) 
        {
            if (_elementsToFind[i] == letter.ToCharArray()[0])
            {
                return i;
            }
        }

        return -1;
    }
}
