﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CubeSpawner : MonoBehaviour {
    
    [FormerlySerializedAs("water")] [SerializeField]
    private GameObject _water;
    [FormerlySerializedAs("pipeBack")] [SerializeField]
    private GameObject _pipeBack;
    [FormerlySerializedAs("pipeFront")] [SerializeField]
    private GameObject _pipeFront;
    [FormerlySerializedAs("defaultYCoordinate")] [SerializeField]
    private float _defaultYCoordinate;
    [SerializeField]
    public int textFontSize = 50;
    private Dictionary<GameObject, string> _cubesArray = new Dictionary<GameObject, string>();
    private bool _shouldSpawnCubesForFrench = false;
    private bool _shouldSpawnCubesForMath = false;
    private int _numberOfElements;
    private char[] _letters;
    private List<float> _numbers;

    void Update()
    {
        if (_shouldSpawnCubesForFrench)
        {
            SpawnLetters();
        }

        if (_shouldSpawnCubesForMath)
        {
            SpawnNumbers();
        }
    }

    public void SetShouldSpawnCubes(bool shouldSpawnCubes, char[] word, char[] extraLetters)
    {
        _shouldSpawnCubesForFrench = shouldSpawnCubes;
        SetLetters(word, extraLetters);
        _numberOfElements = _letters.Length;
    }

    public void SetShouldSpawnCubes(bool shouldSpawnCubes, float number, List<int> extraNumbers)
    {
        this._shouldSpawnCubesForMath = shouldSpawnCubes;
        SetNumbers(number, extraNumbers);
        _numberOfElements = 1 + extraNumbers.Count;
    }

    public void RenewCubes(char[] word, char[] extraLetters)
    {
        EndCubes();
        SetLetters(word, extraLetters);
        _numberOfElements = _letters.Length;
        SpawnLetters();
    }

    public void RenewCubes(float number, List<int> extraNumbers)
    {
        EndCubes();
        SetNumbers(number, extraNumbers);
        _numberOfElements = _numbers.Count;
        SpawnNumbers();
    }

    private void EndCubes()
    {
        foreach (KeyValuePair<GameObject, string> entry in _cubesArray)
        {
            Destroy(entry.Key);
        }

        _cubesArray.Clear();
    }
    
    private void SpawnLetters()
    {
        _shouldSpawnCubesForFrench = false;
        string[] shuffledLetters = GetShuffledArray(_letters);
        for (int i = 1; i <= _numberOfElements; i++)
        {
            Vector3 position = GetPosition(i);
            
            GameObject cube = ObjectPooler.Instance.SpawnFromPool(
                    "Cube",
                    position,
                    Quaternion.identity
             );

            CubeMovement moveScript = cube.GetComponent<CubeMovement>();
            if (moveScript != null)
            {
                moveScript.SetFixedCoordinates(position.x, position.y);
            }
            
            string letter = shuffledLetters[i - 1];
            cube.name = Constants.cubeLetterBaseName + "_" + letter;
            SetupTextMesh(cube, letter);
            _cubesArray.Add(cube, letter);
        }
    }

    private void SpawnNumbers()
    {
        _shouldSpawnCubesForMath = false;
        for (int i = 1; i <= _numberOfElements; i++)
        {
            Vector3 position = GetPosition(i);
            
            GameObject cube = ObjectPooler.Instance.SpawnFromPool(
                "Cube",
                position,
                Quaternion.identity
            );

            CubeMovement moveScript = cube.GetComponent<CubeMovement>();
            if (moveScript != null)
            {
                moveScript.SetFixedCoordinates(position.x, position.y);
            }
            
            string number = _numbers[i - 1].ToString();
            cube.name = Constants.cubeLetterBaseName + "_" + number;
            SetupTextMesh(cube, number);
            _cubesArray.Add(cube, number);
        }
    }

    private Vector3 GetPosition(int index)
    {
        float xPosition = GetCubeXPosition();
        float leftLimit = _pipeFront.transform.position.z;
        float rightLimit = _pipeBack.transform.position.z;
        float initialZPosition = (Mathf.Abs(leftLimit) + Mathf.Abs(rightLimit)) / (_numberOfElements + 1);
        
        Vector3 position = new Vector3();
        position.x = xPosition;
        position.y = _defaultYCoordinate;
        position.z = leftLimit - (initialZPosition * index);

        return position;
    }

    private float GetCubeXPosition()
    {
        return _water.transform.position.x; ;
    }

    private void SetupTextMesh(GameObject cube, string element)
    {
        TextMesh t = cube.GetComponentInChildren<TextMesh>();
        t.text = element;
        t.fontSize = textFontSize;
        t.color = Color.black;
        t.transform.position = new Vector3(
            cube.transform.position.x - (cube.transform.localScale.x / 2), 
            cube.transform.position.y, 
            cube.transform.position.z
        );
        Vector3 eulerAngles = t.transform.rotation.eulerAngles;
        eulerAngles.y = 90;
        t.transform.eulerAngles = eulerAngles;
        t.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    private string[] GetShuffledArray(char[] array)
    {
        string[] shuffledArray = new string[array.Length];
        for (int i = 0; i < array.Length; i++)
        {
            shuffledArray[i] = array[i].ToString().ToUpper();
        }

        for (int t = 0; t < shuffledArray.Length; t++)
        {
            string tmp = shuffledArray[t];
            int r = Random.Range(t, shuffledArray.Length);
            shuffledArray[t] = shuffledArray[r];
            shuffledArray[r] = tmp;
        }

        return shuffledArray;
    }
    
    private List<float> GetShuffledList(List<float> array)
    {
        List<float> shuffledArray = new List<float>();
        for (int i = 0; i < array.Count; i++)
        {
            shuffledArray.Add(array[i]);
        }

        for (int t = 0; t < shuffledArray.Count; t++)
        {
            float tmp = shuffledArray[t];
            int r = Random.Range(t, shuffledArray.Count);
            shuffledArray[t] = shuffledArray[r];
            shuffledArray[r] = tmp;
        }

        return shuffledArray;
    }

    private void SetLetters(char[] wordLetters, char[] extraLetters)
    {
        char[] allLetters = new char[wordLetters.Length + extraLetters.Length];

        for (int i = 0; i < wordLetters.Length; i++)
        {
            allLetters[i] = wordLetters[i];
        }
        
        for (int i = 0; i < extraLetters.Length; i++)
        {
            allLetters[wordLetters.Length + i] = extraLetters[i];
        }

        _letters = allLetters;
    }

    private void SetNumbers(float number, List<int> extraNumbers)
    {
        List<float> numbers = new List<float>();
        numbers.Add(number);
        
        for (int i = 0; i < extraNumbers.Count; i++)
        {
            numbers.Add(extraNumbers[i]);
        }

        this._numbers = GetShuffledList(numbers);
    }
}