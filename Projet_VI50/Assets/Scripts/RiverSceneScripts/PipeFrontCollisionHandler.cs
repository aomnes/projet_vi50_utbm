﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PipeFrontCollisionHandler : MonoBehaviour {

    [FormerlySerializedAs("pipeBack")] [SerializeField]
    private GameObject _pipeBack;

    private void OnCollisionEnter(Collision collision)
    {
        GameObject gameObject = collision.gameObject;
        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x,
            gameObject.transform.position.y,
            _pipeBack.transform.position.z + 1
        );
    }
}
