﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SpritesNames : MonoBehaviour {

    [FormerlySerializedAs("textures")] [SerializeField]
    private Texture2D[] _textures;

    public Texture2D GetTexture(string name)
    {
        foreach(Texture2D texture in _textures) {
            if (texture.name == name)
            {
                return texture;
            }
        }

        Debug.Log("Texture not found.");
        return _textures[0];
    }
}
