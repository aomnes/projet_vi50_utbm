﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class BoxSpawner : MonoBehaviour {
    //Private properties
    [FormerlySerializedAs("boxFrontCube")] [SerializeField] private GameObject _boxFrontCube;
    [FormerlySerializedAs("boxLeftCube")] [SerializeField] private GameObject _boxLeftCube;
    [FormerlySerializedAs("boxRightCube")] [SerializeField] private GameObject _boxRightCube;
    [FormerlySerializedAs("boxContainer")] [SerializeField] private GameObject _boxContainer;
    [FormerlySerializedAs("leftBoxBlocker")] [SerializeField] private GameObject _leftBoxBlocker;
    [FormerlySerializedAs("rightBoxBlocker")] [SerializeField] private GameObject _rightBoxBlocker;
    [FormerlySerializedAs("defaultYCoordinate")] [SerializeField] private float _defaultYCoordinate;
    
    private int _numberOfBoxesToSpawn;
    private bool _shouldSpawnBoxesForFrench;
    private bool _shouldSpawnBoxesForMath;
    private readonly Dictionary<GameObject, string> _boxes = new Dictionary<GameObject, string>();
    private string _wordToFind;
    private float _numberToFind;
    private float _boxZSize;
    private GameType gameType = GameType.French;

    private void OnEnable()
    {
        BoxContainerMovement.OnBoxContainerEnded += SpawnBoxesForFrench;
        BoxContainerMovement.OnBoxContainerEnded += SpawnBoxesForMath;
    }

    private void OnDisable()
    {
        BoxContainerMovement.OnBoxContainerEnded -= SpawnBoxesForFrench;
        BoxContainerMovement.OnBoxContainerEnded -= SpawnBoxesForMath;
    }

    private void Start()
    {
        _boxZSize = _boxFrontCube.transform.localScale.z
                   + _boxLeftCube.transform.localScale.x
                   + _boxRightCube.transform.localScale.x;
    }

    void Update()
    {
        if (_shouldSpawnBoxesForFrench)
        {
            ScaleBoxContainer();
            setBoxBlockers();
            SpawnBoxesForFrench();
        }

        if (_shouldSpawnBoxesForMath)
        {
            ScaleBoxContainer();
            setBoxBlockers();
            SpawnBoxesForMath();
        }
    }

    public void SetShouldSpawnBoxes(bool shouldSpawnBoxes, string word)
    {
        gameType = GameType.French;
        this._shouldSpawnBoxesForFrench = shouldSpawnBoxes;
        _wordToFind = word;
        _numberOfBoxesToSpawn = word.Length;
    }

    public void SetShouldSpawnBoxes(bool shouldSpawnBoxes, float elementToGuess)
    {
        gameType = GameType.Math;
        this._shouldSpawnBoxesForMath = shouldSpawnBoxes;
        _numberToFind = elementToGuess;
        _numberOfBoxesToSpawn = 1;
    }

    public void RenewBoxes(string word)
    {
        EndBoxes();
        _wordToFind = word;
        _numberOfBoxesToSpawn = word.Length;
    }

    public void RenewBoxes(float number)
    {
        EndBoxes();
        _numberToFind = number;
        _numberOfBoxesToSpawn = 1;
    }

    private void EndBoxes()
    {
        BoxContainerMovement moveScript = _boxContainer.GetComponent<BoxContainerMovement>();
        moveScript.EndBoxes();
        _boxes.Clear();
    }

    private void ScaleBoxContainer()
    {
        _boxContainer.transform.localScale = new Vector3(
            _boxLeftCube.transform.localScale.z,
            _boxFrontCube.transform.localScale.y,
            _boxZSize * _numberOfBoxesToSpawn
        );
    }

    private void SpawnBoxesForFrench()
    {
        if (gameType != GameType.French)
        {
            return;
        }
        
        _shouldSpawnBoxesForFrench = false;
        Vector3 position = new Vector3();
        char[] wordLetters = _wordToFind.ToCharArray();

        for (int i = 1; i <= _numberOfBoxesToSpawn; i++)
        {
            position.y = _defaultYCoordinate;
            position.x = GetBoxXCoordinate(i);
            position.z = GetBoxZCoordinate(i);

            GameObject key = ObjectPooler.Instance.SpawnInParentFromPool(
                "Box",
                position,
                _boxContainer.transform,
                Quaternion.identity
            );
            
            key.transform.localEulerAngles = new Vector3(0, 180, 0);
            
            string letter = wordLetters[i - 1].ToString().ToUpper();
            key.name = Constants.boxBaseName + "_" + letter;
            _boxes.Add(
                key,
                letter
            );
        }
        
        BoxContainerMovement moveScript = _boxContainer.GetComponent<BoxContainerMovement>();
        moveScript.StartBoxes();
    }

    private void SpawnBoxesForMath()
    {
        if (gameType != GameType.Math)
        {
            return;
        }
        
        _shouldSpawnBoxesForMath = false;
        Vector3 position = new Vector3();

        position.y = _defaultYCoordinate;
        position.x = GetBoxXCoordinate(1);
        position.z = GetBoxZCoordinate(1);

        GameObject key = ObjectPooler.Instance.SpawnInParentFromPool(
            "Box",
            position,
            _boxContainer.transform,
            Quaternion.identity
        );
            
        key.transform.localEulerAngles = new Vector3(0, 180, 0);

        key.name = Constants.boxBaseName + "_" + _numberToFind;
        _boxes.Add(
            key,
            _numberToFind.ToString()
        );
        
        BoxContainerMovement moveScript = _boxContainer.GetComponent<BoxContainerMovement>();
        moveScript.StartBoxes();
    }

    private void setBoxBlockers()
    {
        float limit = 0;
        if (_numberOfBoxesToSpawn % 2 == 0)
        {
            limit = _numberOfBoxesToSpawn * _boxZSize;
        }
        else
        {
            limit = ((_numberOfBoxesToSpawn - 1) * _boxZSize) + (_boxZSize / 2);
        }
        
        _leftBoxBlocker.transform.position = new Vector3(
            _leftBoxBlocker.transform.position.x,
            _leftBoxBlocker.transform.position.y,
            -limit
        );
        _rightBoxBlocker.transform.position = new Vector3(
            _rightBoxBlocker.transform.position.x,
            _rightBoxBlocker.transform.position.y,
            limit
        );
    }

    private float GetBoxZCoordinate(int boxIndex)
    {
        if ((_numberOfBoxesToSpawn % 2) == 0)
        {
            if (boxIndex == (_numberOfBoxesToSpawn / 2))
            {
                return 0 - GetBoxZScale() / 2;
            } 
            else if (boxIndex == (_numberOfBoxesToSpawn / 2) + 1)
            {
                return 0 + GetBoxZScale() / 2;
            }
            if (boxIndex < (_numberOfBoxesToSpawn / 2))
            {
                return 0 - (GetBoxZScale() / 2) - (GetBoxZScale() * ((_numberOfBoxesToSpawn / 2)  - boxIndex));
            }
            else if (boxIndex > (_numberOfBoxesToSpawn / 2))
            {
                return 0 + (GetBoxZScale() / 2) + (GetBoxZScale() * (boxIndex - ((_numberOfBoxesToSpawn / 2) + 1)));
            }
        }
        else
        {
            int median = (1 + _numberOfBoxesToSpawn) / 2;
            if (boxIndex == median)
            {
                return 0;
            }
            else if (boxIndex < median) 
            {
                return 0 - (median - boxIndex) * GetBoxZScale();
            }
            else if (boxIndex > median)
            {
                return 0 + (boxIndex - median) * GetBoxZScale();
            }
        }

        return 0;
    }

    private float GetBoxXCoordinate(int boxIndex)
    {
        return transform.position.x;
    }

    private float GetBoxZScale()
    {
        return _boxFrontCube.GetComponent<Renderer>().bounds.size.z 
            + _boxLeftCube.GetComponent<Renderer>().bounds.size.z 
            + _boxRightCube.GetComponent<Renderer>().bounds.size.z;
    }
}
