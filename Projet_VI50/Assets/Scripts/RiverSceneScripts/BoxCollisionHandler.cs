using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class BoxCollisionHandler : MonoBehaviour {

    [FormerlySerializedAs("frontCube")] [SerializeField] private GameObject _frontCube;
    [FormerlySerializedAs("cubeBoxCollisionSound")] [SerializeField] private AudioSource _cubeBoxCollisionSound;
    private RiverGameController _gameController;
    private GameObject previousCollision;

    public int textFontSize = 50;

    private void Start()
    {
        GameObject riverGameController = GameObject.Find(Constants.riverGameControllerName);
        _gameController = (RiverGameController) riverGameController.GetComponent(typeof(RiverGameController));
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Contains(Constants.cubeLetterBaseName))
        {
            if (previousCollision != null)
            {
                if (previousCollision == collision.gameObject)
                {
                    return;
                }
            }

            previousCollision = collision.gameObject;
            _cubeBoxCollisionSound.Play();
            string parentName = transform.parent.name;
            string boxElement= parentName.Split('_')[1];
            string cubeElement = collision.gameObject.name.Split('_')[1];
            bool isLetterCorrect = string.Equals(boxElement, cubeElement);
            ColorGameObject(isLetterCorrect ? Color.green : Color.red);
            DrawElement(collision.gameObject.name.Split('_')[1]);
            
            bool isBoxNumber = boxElement.All(char.IsDigit);
            bool isCubeNumber = cubeElement.All(char.IsDigit);

            if (isBoxNumber && isCubeNumber)
            {
                _gameController.OnBoxFilled(int.Parse(cubeElement));
            }
            else
            {
                _gameController.OnBoxFilled(cubeElement, boxElement);   
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {

        if (collision.gameObject.name.Contains(Constants.cubeLetterBaseName))
        {
            string parentName = transform.parent.name;
            string boxElement = parentName.Substring(transform.parent.name.Length - 1);
            ColorGameObject(Color.white);
            EraseLetter();

            if (!boxElement.All(char.IsDigit))
            {
                _gameController.OnBoxEmptied(boxElement);  
            } 
        }
    }

    private void ColorGameObject(Color c)
    {
        foreach (Renderer r in transform.parent.GetComponentsInChildren<Renderer>())
        {
            r.material.color = c;
        }
    }

    private void DrawElement(string element)
    {  
        TextMesh t = _frontCube.GetComponentInChildren<TextMesh>();
        t.text = element;
        t.fontSize = textFontSize;
        t.color = Color.black;
        t.transform.position = new Vector3(
            _frontCube.transform.position.x,
            _frontCube.transform.position.y,
            _frontCube.transform.position.z
        );
        Vector3 eulerAngles = t.transform.rotation.eulerAngles;
        eulerAngles.y = -90;
        t.transform.eulerAngles = eulerAngles;
        t.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        t.anchor = TextAnchor.MiddleCenter;
    }

    private void EraseLetter()
    {
        TextMesh t = _frontCube.GetComponentInChildren<TextMesh>();
        t.text = "";
    }
}
