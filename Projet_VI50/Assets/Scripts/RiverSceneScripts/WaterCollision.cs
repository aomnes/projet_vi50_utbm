﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WaterCollision : MonoBehaviour
{

    [FormerlySerializedAs("dropObjectInRiverSound")] [SerializeField] private AudioSource _dropObjectInRiverSound;

    private void OnCollisionEnter(Collision other)
    {
        _dropObjectInRiverSound.transform.position = other.gameObject.transform.position;
        _dropObjectInRiverSound.Play();
    }

    private void OnCollisionStay(Collision collision)
    {
        CubeMovement moveScript = collision.gameObject.GetComponent<CubeMovement>();
        if (moveScript != null)
        {
            moveScript.MoveOnRiver();
        }
    }
}
