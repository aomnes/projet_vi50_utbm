﻿using System;
using Random = UnityEngine.Random;

public class FrenchGameDataHelper
{
    private static string[] _cpLevelWords = new string[]
    {
        "maison",
        "voiture",
        "gâteau",
        "chat",
        "chien",
        "loup",
        "pont",
        "vache"
    };
    private static string[] _ce1LevelWords = new string[]
    {
        "maison",
        "voiture",
        "gâteau",
        "chat",
        "chien",
        "loup",
        "pont",
        "vache",
        "avion",
        "bouteille",
        "france",
        "pain"
    };
    private static string[] _ce2LevelWords = new string[]
    {
        "maison",
        "voiture",
        "gâteau",
        "chat",
        "chien",
        "loup",
        "pont",
        "vache",
        "avion",
        "bouteille",
        "france",
        "pain",
        "bâteau",
        "sapin",
        "camion"
    };
    private static string[] _cm1LevelWords = new string[]
    {
        "maison",
        "voiture",
        "gâteau",
        "chat",
        "chien",
        "loup",
        "pont",
        "vache",
        "avion",
        "bouteille",
        "france",
        "pain",
        "bâteau",
        "sapin",
        "moto",
        "volcan",
        "planète",
        "camion"
    };
    private static string[] _cm2LevelWords = new string[]
    {
        "maison",
        "voiture",
        "gâteau",
        "chat",
        "chien",
        "loup",
        "pont",
        "vache",
        "avion",
        "bouteille",
        "france",
        "pain",
        "bâteau",
        "sapin",
        "moto",
        "volcan",
        "planète",
        "hélicoptère",
        "bicyclette",
        "camion"
    };

    public static string[] GetWords(SchoolLevels level)
    {
        int numberOfWords = 3;
        switch (level)
        {
            case SchoolLevels.CP:
                numberOfWords = 3;
                break;
            case SchoolLevels.CE1:
                numberOfWords = 5;
                break;
            case SchoolLevels.CE2:
                numberOfWords = 6;
                break;
            case SchoolLevels.CM1:
                numberOfWords = 8;
                break;
            case SchoolLevels.CM2:
                numberOfWords = 9;
                break;
        }
        
        string[] selectedWords = new string[numberOfWords];

        for (int i = 0; i < numberOfWords; i++)
        {
            string randomWord = GetRandomWord(level);
            int pos = Array.IndexOf(selectedWords, randomWord);
            while (pos > -1)
            {
                randomWord = GetRandomWord(level);
                pos = Array.IndexOf(selectedWords, randomWord);
            }
            selectedWords[i] = randomWord;
        }

        return selectedWords;
    }

    public static char[] GetRandomLetters(SchoolLevels level)
    {
        int numberOfLetters = 3;
        
        switch (level)
        {
            case SchoolLevels.CP:
                numberOfLetters = 3;
                break;
            case SchoolLevels.CE1:
                numberOfLetters = 5;
                break;
            case SchoolLevels.CE2:
                numberOfLetters = 6;
                break;
            case SchoolLevels.CM1:
                numberOfLetters = 7;
                break;
            case SchoolLevels.CM2:
                numberOfLetters = 8;
                break;
        }

        return GenerateLetters(numberOfLetters);
    }
    
    private static char[] GenerateLetters(int numberOfLetters)
    {
        char[] letters = new char[numberOfLetters];
        for (int i = 0; i < numberOfLetters; i++)
        {
            int letterIndex = Random.Range(0, 26);
            char letter = (char) ('a' + letterIndex);
            letters[i] = letter;
        }

        return letters;
    }

    private static string GetRandomWord(SchoolLevels level)
    {
        int wordIndex = 0;

        switch (level)
        {
            case SchoolLevels.CP:
                wordIndex = Random.Range(0, _cpLevelWords.Length);
                return _cpLevelWords[wordIndex];
            case SchoolLevels.CE1:
                wordIndex = Random.Range(0, _ce1LevelWords.Length);
                return _ce1LevelWords[wordIndex];
            case SchoolLevels.CE2:
                wordIndex = Random.Range(0, _ce2LevelWords.Length);
                return _ce2LevelWords[wordIndex];
            case SchoolLevels.CM1:
                wordIndex = Random.Range(0, _cm1LevelWords.Length);
                return _cm1LevelWords[wordIndex];
            case SchoolLevels.CM2:
                wordIndex = Random.Range(0, _cm2LevelWords.Length);
                return _cm2LevelWords[wordIndex];
            default:
                wordIndex = Random.Range(0, _cpLevelWords.Length);
                return _cpLevelWords[wordIndex];
        }
    }
}