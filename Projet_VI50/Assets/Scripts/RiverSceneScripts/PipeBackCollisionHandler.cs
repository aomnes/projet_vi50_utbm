﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PipeBackCollisionHandler : MonoBehaviour {

    [FormerlySerializedAs("pipeFront")] [SerializeField]
    private GameObject _pipeFront;

    private void OnCollisionEnter(Collision collision)
    {
        GameObject gameObject = collision.gameObject;
        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x,
            gameObject.transform.position.y,
            _pipeFront.transform.position.z - 1
        );
    }
}
