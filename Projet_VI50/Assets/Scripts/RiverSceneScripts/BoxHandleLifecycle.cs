﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxHandleLifecycle : MonoBehaviour
{

	private BoxContainerGrabbable _grabbableScript;
	
	void OnEnable()
	{
        _grabbableScript = GameObject.Find("BoxContainer").GetComponent<BoxContainerGrabbable>();

        if (_grabbableScript != null)
		{
			_grabbableScript.AddGrabPoints(GetComponent<Collider>());	
		}
	}

	private void OnDisable()
	{
		if (_grabbableScript != null)
		{
			_grabbableScript.RemoveGrabPoints(GetComponent<Collider>());	
		}	
	}
}
