﻿using UnityEngine;
using UnityEngine.Serialization;

public class CubeMovement : MonoBehaviour
{
    [FormerlySerializedAs("movingSpeedMultiplier")] [SerializeField] private float _movingSpeedMultiplier = 1.5f;
    private float _defaultX;
    private float _defaultY;
    private bool _shouldMoveToRiver = false;
    private OVRGrabbable _grabbable;

    private void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
    }

    private void FixedUpdate()
    {
        if (_shouldMoveToRiver)
        {
            MoveToRiver();
        }
    }
    
    
    public void SetFixedCoordinates(float x, float y)
    {
        _defaultX = x;
        _defaultY = y;
    }

    public void MoveOnRiver()
    {
        _shouldMoveToRiver = false;
        _grabbable.enabled = true;
        
        if (transform.position.x != _defaultX)
        {
            transform.position = new Vector3(
                _defaultX,
                transform.position.y,
                transform.position.z
            );
        }
        
        SetDefaultRotation();
        transform.Translate(Vector3.back * Time.deltaTime * _movingSpeedMultiplier);
    }

    public void StartMoveToRiver()
    {
        _shouldMoveToRiver = true;
        _grabbable.enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().useGravity = false;
        SetDefaultRotation();
    }

    private void SetDefaultRotation()
    {
        if (transform.localEulerAngles != new Vector3(0, 0, 0))
        {
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }

    private void MoveToRiver()
    {
        if (transform.position.x == _defaultX && transform.position.y >= _defaultY)
        {
            _shouldMoveToRiver = false;
            GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Rigidbody>().useGravity = true;
        }
        else if (transform.position.y <= _defaultY)
        {
            transform.Translate(Vector3.up * Time.deltaTime);   
        }
        else if (transform.position.x < _defaultX + 0.2 && transform.position.x > _defaultX - 0.2)
        {
            transform.position = new Vector3(
                _defaultX,
                transform.position.y,
                transform.position.z
            );
        }
        else
        {
            float xMove = Mathf.Abs(transform.position.x) - Mathf.Abs(_defaultX) > 0 
                ? 1 : -1;
            Vector3 moveVector = new Vector3(xMove, 0, 0);
            transform.Translate(moveVector * Time.deltaTime);
        }
    }
}