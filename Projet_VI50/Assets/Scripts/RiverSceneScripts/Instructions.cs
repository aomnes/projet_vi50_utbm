﻿public class Instructions
{
    public static string frenchGameInstructions =
        "Le but du jeu est de reconstituer le mot \n  " +
        "décrivant l'objet que vous verrez. \n" +
        "Pour cela, il faut poser les cubes flottant \n" +
        "dans la rivière dans les boites derrière vous. \n" +
        "Appuyez sur la gâchette pour démarrer. \n" +
        "Pour abandonner, réappuyez sur la gâchette \n" +
        "une fois la partie démarée.";
    public static string mathGameInstructions =
        "Le but du jeu est de compléter le calcul \n  " +
        "que vous verrez. \n" +
        "Pour cela, il faut poser les cubes flottant \n" +
        "dans la rivière dans les boites derrière vous. \n" +
        "Appuyez sur la gâchette pour démarrer. \n" +
        "Pour abandonner, réappuyez sur la gâchette \n" +
        "une fois la partie démarée.";

    public static string ropeFormWordInstructions =
        "Le but du jeu est d'associer les éléments\n" +
        "entre eux. Il faut les relier en attrapant\n" +
        "les bouts de corde et les placer sur le\n" +
        "cube correspondant. \n" +
        "Pour démarrer, appuyez sur le bouton B.\n" +
        "Pour quitter, appuyez simultanément sur les\n" +
        "boutons A et X ou B et Y";

    public static string ropeFormInstructions =
        "Le but du jeu est d'associer les éléments\n" +
        "entre eux. Il faut les relier en attrapant\n" +
        "les bouts de corde et les placer sur le\n" +
        "cube correspondant. \n" +
        "Pour démarrer, appuyez sur le bouton B.\n" +
        "Pour quitter, appuyez simultanément sur les\n" +
        "boutons A et X ou B et Y";

    public static string ropeMathInstructions =
        "Le but du jeu est d'associer les éléments\n" +
        "entre eux. Il faut les relier en attrapant\n" +
        "les bouts de corde et les placer sur le\n" +
        "cube correspondant. \n" +
        "Pour démarrer, appuyez sur le bouton B.\n" +
        "Pour quitter, appuyez simultanément sur les\n" +
        "boutons A et X ou B et Y";

    public static string endGame =
        "FELICITATIONS, vous avez terminé ! \n" +
        "vous allez être redirigé au\n" +
        "menu principal dans 5 secondes.";

    public static string leaveGame =
        "Vous avez abandonné, vous allez être\n" +
        "redirigé au menu dans 5 secondes";
}
