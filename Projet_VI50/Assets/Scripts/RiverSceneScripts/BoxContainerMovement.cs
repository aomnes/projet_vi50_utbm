﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class BoxContainerMovement : MonoBehaviour {
	
	[FormerlySerializedAs("minYPosition")] [SerializeField] private float _minYPosition;
	[FormerlySerializedAs("maxYPosition")] [SerializeField] private float _maxYPosition;

	private bool _shouldStart;
	private bool _shouldEnd;
	
	public delegate void BoxContainerAction();
	public static event BoxContainerAction OnBoxContainerEnded;

	private void FixedUpdate()
	{
		if (_shouldStart)
		{
			MoveUp();
		}
		else if (_shouldEnd)
		{
			MoveDown();
		}
	}

	public void StartBoxes()
	{
		_shouldStart = true;
		_shouldEnd = false;
	}

	public void EndBoxes()
	{
		_shouldStart = false;
		_shouldEnd = true;
	}

	private void MoveUp()
	{
		if (transform.position.y >= _maxYPosition)
		{
			_shouldStart = false;
			transform.position = new Vector3(
				transform.position.x,
				_maxYPosition,
				transform.position.z
			);
			return;
		}

		transform.Translate(Vector3.up * Time.deltaTime);
	}

	private void MoveDown()
	{
		if (transform.position.y <= _minYPosition)
		{
			_shouldEnd = false;
			foreach (Transform t in GetComponentInChildren<Transform>())
			{
				Destroy(t.gameObject);
			}

			if (OnBoxContainerEnded != null)
			{
				OnBoxContainerEnded();	
			}
			
			return;
		}

		transform.Translate(Vector3.down * Time.deltaTime);
	}
}
