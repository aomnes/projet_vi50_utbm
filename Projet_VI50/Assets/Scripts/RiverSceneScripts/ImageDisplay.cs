﻿using UnityEngine;
using UnityEngine.Serialization;

public class ImageDisplay : MonoBehaviour {

    [FormerlySerializedAs("spritesNames")] [SerializeField]
    private SpritesNames _spritesNames;
    private SpriteRenderer _spriteRenderer;
    private readonly int _maxWidth = 250;
    private readonly int _maxHeight = 250;
    private Vector3 defaultLocalScale;

    public void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        defaultLocalScale = _spriteRenderer.transform.localScale;
    }

    public void SetImage(string imageName)
    {
        Texture2D texture = _spritesNames.GetTexture(imageName);
        Sprite newSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
        _spriteRenderer.sprite = newSprite;

        _spriteRenderer.transform.localScale =  defaultLocalScale * GetScale(texture.width, texture.height);
    }

    private float GetScale(int textureWidth, int textureHeight)
    {
        return (textureWidth > textureHeight) ?
            _maxWidth / (float)textureWidth :
            _maxHeight / (float)textureHeight;
    }
}
