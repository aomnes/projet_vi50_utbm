﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class FloorCollisionHandler : MonoBehaviour
{

	[FormerlySerializedAs("cubeFloorCollisionSound")] [SerializeField] private AudioSource _cubeFloorCollisionSound;
	
	private void OnCollisionEnter(Collision other)
	{
		CubeMovement cubeScript = other.gameObject.GetComponent<CubeMovement>();
		if (cubeScript != null)
		{
			_cubeFloorCollisionSound.Play();
			cubeScript.StartMoveToRiver();
		}
	}
}
