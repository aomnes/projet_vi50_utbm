﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MathGameDataHelper
{

    private const int NumberOfCalculationsForCp = 5;
    private const int NumberOfCalculationsForCe1 = 6;
    private const int NumberOfCalculationsForCe2 = 6;
    private const int NumberOfCalculationsForCm1 = 7;
    private const int NumberOfCalculationsForCm2 = 7;
    
    public static List<Calculation> GetCalculations(SchoolLevels level)
    {
        switch (level)
        {
            case SchoolLevels.CP:
                return GetCpLevelCalculations();
            case SchoolLevels.CE1:
                return GetCe1LevelCalculations();
            case SchoolLevels.CE2:
                return GetCe2LevelCalculations();
            case SchoolLevels.CM1:
                return GetCm1LevelCalculations();
            case SchoolLevels.CM2:
                return GetCm2LevelCalculations();
        }

        throw new Exception("Level not supported.");
    }

    public static List<int> GetRandomNumbers(SchoolLevels level)
    {
        List<int> randomNumbers = new List<int>();
        
        switch (level)
        {
            case SchoolLevels.CP:
                for (int i = 0; i < 5; i++)
                {
                    randomNumbers.Add(Random.Range(1,25));
                }
                return randomNumbers;
            case SchoolLevels.CE1:
                for (int i = 0; i < 5; i++)
                {
                    randomNumbers.Add(Random.Range(1,50));
                }
                return randomNumbers;
            case SchoolLevels.CE2:
                for (int i = 0; i < 5; i++)
                {
                    randomNumbers.Add(Random.Range(1,50));
                }
                return randomNumbers;
            case SchoolLevels.CM1:
                for (int i = 0; i < 5; i++)
                {
                    randomNumbers.Add(Random.Range(1,50));
                }
                return randomNumbers;
            case SchoolLevels.CM2:
                for (int i = 0; i < 5; i++)
                {
                    randomNumbers.Add(Random.Range(1,50));
                }
                return randomNumbers;
        }

        throw new Exception("Level not supported.");
    }

    private static List<Calculation> GetCpLevelCalculations()
    {
        List<Calculation> calculations = new List<Calculation>();
        
        for (int i = 0; i < NumberOfCalculationsForCp; i++)
        {
            int number1 = Random.Range(1, 25);
            int number2 = Random.Range(1, 25);
            Calculation c = new Calculation(
                number1,
                number2,
                GetOperator(SchoolLevels.CP)
            );
            calculations.Add(c);
        }

        return calculations;
    }
    
    private static List<Calculation> GetCe1LevelCalculations()
    {
        List<Calculation> calculations = new List<Calculation>();
        
        for (int i = 0; i < NumberOfCalculationsForCe1; i++)
        {
            int number1 = Random.Range(1, 50);
            int number2 = Random.Range(1, number1);
            Calculation c = new Calculation(
                number1,
                number2,
                GetOperator(SchoolLevels.CE1)
            );
            calculations.Add(c);
        }

        return calculations;
    }
    
    private static List<Calculation> GetCe2LevelCalculations()
    {
        List<Calculation> calculations = new List<Calculation>();
        
        for (int i = 0; i < NumberOfCalculationsForCe2; i++)
        {
            int number1 = Random.Range(1, 50);
            int number2 = Random.Range(1, number1);
            Calculation c = new Calculation(
                number1,
                number2,
                GetOperator(SchoolLevels.CE2)
            );
            calculations.Add(c);
        }

        return calculations;
    }
    
    private static List<Calculation> GetCm1LevelCalculations()
    {
        List<Calculation> calculations = new List<Calculation>();
        
        for (int i = 0; i < NumberOfCalculationsForCm1; i++)
        {
            Operator o = GetOperator(SchoolLevels.CM1);
            calculations.Add(GetAdvancedCalculation(o));
        }

        return calculations;
    }
    
    private static List<Calculation> GetCm2LevelCalculations()
    {
        List<Calculation> calculations = new List<Calculation>();
        
        for (int i = 0; i < NumberOfCalculationsForCm2; i++)
        {
            Operator o = GetOperator(SchoolLevels.CM2);
            calculations.Add(GetAdvancedCalculation(o));
        }

        return calculations;
    }

    //Returns a calculation not too hard for a kid based on the operator
    private static Calculation GetAdvancedCalculation(Operator o)
    {
        int number1 = 0;
        int number2 = 0;

        if (o == Operator.addition || o == Operator.subtraction)
        {
            number1 = Random.Range(1, 50);
            number2 = Random.Range(1, number1);
        }

        if (o == Operator.multiplication)
        {
            number1 = Random.Range(1, 10);
            number2 = Random.Range(1, 10);
        }

        if (o == Operator.division)
        {
            number1 = Random.Range(1, 50);
            do
            {
                number2 = Random.Range(1, 10);
            } while (((number1 / number2) != (int) (number1 / number2)) && (number1 / number2) != 0);
        }

        return new Calculation(
            number1,
            number2,
            o
        );
    }

    private static Operator GetOperator(SchoolLevels level)
    {
        int index = 1;

        switch (level)
        {
            case SchoolLevels.CE1:
                index = Random.Range(1, 2);
                break;
            case SchoolLevels.CE2:
                index = Random.Range(1, 2);
                break;
            case SchoolLevels.CM1:
                index = Random.Range(1, 4);
                break;
            case SchoolLevels.CM2:
                index = Random.Range(1, 4);
                break;
        }
        
        if (index == 1)
        {
            return Operator.addition;
        } 
        if (index == 2)
        {
            return Operator.subtraction;
        } 
        if (index == 3)
        {
            return Operator.division;
        }
        return Operator.multiplication;
    }
}
