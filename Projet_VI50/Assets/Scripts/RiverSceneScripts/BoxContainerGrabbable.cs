﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoxContainerGrabbable : OVRGrabbable {
	void Awake() {}

	/*public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
	{
		base.GrabBegin(hand, grabPoint);
		GetComponent<Rigidbody>().isKinematic = false;
	}

	public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
	{
		base.GrabEnd(linearVelocity, angularVelocity);
		GetComponent<Rigidbody>().isKinematic = true;
	}*/

	public void AddGrabPoints(Collider newCollider)
	{
		Collider[] grabPoints = m_grabPoints;
		
		Collider[] updatedGrabPoints = new Collider[grabPoints.Length + 1];
		for (int i = 0; i < m_grabPoints.Length; i++)
		{
			updatedGrabPoints[i] = m_grabPoints[i];
		}

		updatedGrabPoints[grabPoints.Length] = newCollider;
		m_grabPoints = updatedGrabPoints;
	}

	public void RemoveGrabPoints(Collider obsoleteCollider)
	{
		Collider[] grabPoints = m_grabPoints;
		List<Collider> colliders = grabPoints.ToList();
		colliders.Remove(obsoleteCollider);

		m_grabPoints = colliders.ToArray();
	}
}