﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Calculation
{
    private readonly int _number1;
    private readonly int _number2;
    private readonly Operator _calculationOperator;
    private readonly float _elementToGuess;

    public Calculation(int number1, int number2, Operator calculationOperator)
    {
        this._number1 = number1;
        this._number2 = number2;
        this._calculationOperator = calculationOperator;
        _elementToGuess = ElementToGuess();
    }

    public int Number1
    {
        get { return _number1; }
    }

    public int Number2
    {
        get { return _number2; }
    }

    public Operator CalculationOperator
    {
        get { return _calculationOperator; }
    }

    public float GetElementToGuess
    {
        get { return _elementToGuess; }
    }

    public float GetResult()
    {
        switch (_calculationOperator)
        {
            case Operator.addition:
                return _number1 + _number2;
            case Operator.subtraction:
                return _number1 - _number2;
            case Operator.division:
                return _number1 / _number2;
            case Operator.multiplication:
                return _number1 * _number2;
        }

        throw new Exception("Operator not supported.");
    }

    private float ElementToGuess()
    {
        int index = Random.Range(1, 3);
        switch (index)
        {
            case 1:
                return _number1;
            case 2:
                return _number2;
            case 3:
                return GetResult();
        }

        throw new Exception("Issue with element to guess.");
    }

    public bool isEqual(Calculation c)
    {
        return c._number1 == _number1 
            && c._number2 == _number2
            && c.GetResult() == GetResult();
    }

    public string ToString()
    {
        string s = "";

        if (_elementToGuess == _number1)
        {
            s += "...";
        }
        else
        {
            s += _number1.ToString();
        }

        switch (_calculationOperator)
        {
            case Operator.addition:
                s += " + ";
                break;
            case Operator.subtraction:
                s += " - ";
                break;
            case Operator.division:
                s += " / ";
                break;
            case Operator.multiplication:
                s += " * ";
                break;
        }

        if (_elementToGuess == _number2 && _number1 != _number2)
        {
            s += "...";
        }
        else
        {
            s += _number2.ToString();
        }

        s += " = ";

        if (_elementToGuess == GetResult())
        {
            s += "...";
        }
        else
        {
            s += GetResult().ToString();
        }

        return s;
    }
}

public enum Operator
{
    addition,
    subtraction,
    division,
    multiplication
}