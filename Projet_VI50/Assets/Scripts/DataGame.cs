﻿using System;

public class DataGame : Singleton<DataGame> {
    
    public DataPlayer DataPlayer;
    public bool IsConfigurationDone;
    public MiniJeuScriptableObject CurrentGame;
    
    public MiniJeuScriptableObject[] MiniJeux;

    private void Start()
    {
        DataPlayer = new DataPlayer(SchoolLevels.None, string.Empty);
        //TODO Peut-être il vaudra mieux utiliser un scriptable object
        DontDestroyOnLoad(this);
    }

    public MiniJeuScriptableObject GetMiniJeu(string nameJeu)
    {
        foreach (var jeu in MiniJeux)
        {
            if (jeu.name.Equals(nameJeu)) return jeu;
        }

        return null;
    }
}

[Serializable]
public struct DataPlayer
{
    public SchoolLevels NameClasseStudent;
    public string NameMascotSelected;

    public DataPlayer(SchoolLevels nameClasseStudent, string nameMascotSelected)
    {
        this.NameClasseStudent = nameClasseStudent;
        this.NameMascotSelected = nameMascotSelected;
    }
}
