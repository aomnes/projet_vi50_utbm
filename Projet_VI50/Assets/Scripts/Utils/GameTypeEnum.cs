﻿// Game types
public enum GameType
{
    Math,
    FormWord,
    Form,
    French,
    None
}