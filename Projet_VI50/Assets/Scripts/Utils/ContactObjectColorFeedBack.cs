﻿using UnityEngine;

public class ContactObjectColorFeedBack : MonoBehaviour
{	
	private Color _colorObject;
	private readonly Color _contactColorObject = Color.cyan;

	private Material _material;

	private void Start()
	{
		_material = this.GetComponent<Renderer>().material;
	}

	private void OnCollisionEnter(Collision other)
	{
		if (!IsHandCollider(other.collider)) return;
		_colorObject = _material.color;
		_material.color = _contactColorObject;
	}

	private void OnCollisionExit(Collision other)
	{
		if (!IsHandCollider(other.collider)) return;
		_material.color = _colorObject;
	}

	private bool IsHandCollider(Collider collider)
	{
		OVRGrabber ovrGrabber = collider.GetComponentInParent<OVRGrabber>();
		if (ovrGrabber != null) return true;
		ovrGrabber = collider.GetComponent<OVRGrabber>();
		if (ovrGrabber != null) return true;
		return false;
	}
}
