﻿using UnityEngine;

public class CheatCode : MonoBehaviour {
	
	private void Start()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	private void Update()
	{
		if (ActionButtonTouch.IsActiveButtonCheatCodeAllGameDoneRestart()) AllGameDoneRestartMainMenu();
		if (ActionButtonTouch.IsActiveButtonResetOrientation()) ResetOrientation();
		if (ActionButtonTouch.IsActiveButtonQuitApplication()) QuitGame();
	}

	[ContextMenu("AllGameDoneRestartMainMenu")]
	public void AllGameDoneRestartMainMenu()
	{
		if (!DataGame.Instance.IsConfigurationDone)
			DataGame.Instance.IsConfigurationDone = true;
		foreach (var miniJeuScriptableObject in DataGame.Instance.MiniJeux)
		{
			miniJeuScriptableObject.IsDone = true;
		}
		LevelManager.LoadMain();
	}

	[ContextMenu("ResetOrientation")]
	public void ResetOrientation()
	{
		//*************************
		// reset orientation
		//*************************
		OVRManager.display.RecenterPose();
	}

	private void QuitGame()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}
