﻿using UnityEngine;

public class ActionButtonTouch {

	public static bool IsActiveButtonStartGame()
	{
		return (OVRInput.GetDown(OVRInput.RawButton.B) || Input.GetKeyDown(KeyCode.A));
	}
	
	public static void GiveUp()
	{
		if ((OVRInput.Get(OVRInput.RawButton.A) && OVRInput.Get(OVRInput.RawButton.X)) 
		    || (OVRInput.Get(OVRInput.RawButton.B) && OVRInput.Get(OVRInput.RawButton.Y))
		    || Input.GetKeyDown(KeyCode.G))
		{
			LevelManager.LoadMain();
		}
	}

	public static bool IsActiveButtonCheatCodeAllGameDoneRestart()
	{
		return (Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.P) && Input.GetKey(KeyCode.I));
	}

	public static bool IsActiveButtonResetOrientation()
	{
		return (Input.GetKey(KeyCode.N) && Input.GetKey(KeyCode.M));
	}

	public static bool IsActiveButtonQuitApplication()
	{
		return (Input.GetKey(KeyCode.Q));
	}
}
