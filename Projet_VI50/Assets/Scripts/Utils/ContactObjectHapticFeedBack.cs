﻿using UnityEngine;

public class ContactObjectHapticFeedBack : MonoBehaviour
{

    [SerializeField] private AudioClip _audioClip;

    private OVRHapticsClip _hapticsClips;

    private void Start()
    {
        _hapticsClips = new OVRHapticsClip(_audioClip);
    }

    private void OnCollisionEnter(Collision other)
    {
        OVRGrabber ovrGrabber = IsHandCollider(other.collider);
        if (ovrGrabber != null)
        {
            if (ovrGrabber.Controller == OVRInput.Controller.LTouch)
            {
                OVRHaptics.LeftChannel.Preempt(_hapticsClips);
            }
            else if (ovrGrabber.Controller == OVRInput.Controller.RTouch)
            {
                OVRHaptics.RightChannel.Preempt(_hapticsClips);
            }
        }
    }

    private OVRGrabber IsHandCollider(Collider collider)
    {
        OVRGrabber ovrGrabber = collider.GetComponentInParent<OVRGrabber>();
        if (ovrGrabber != null) return ovrGrabber;
        ovrGrabber = collider.GetComponent<OVRGrabber>();
        if (ovrGrabber != null) return ovrGrabber;
        return null;
    }
}
