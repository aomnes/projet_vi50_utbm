﻿public enum SchoolLevels
{
    CP,
    CE1,
    CE2,
    CM1,
    CM2,
    None
}
