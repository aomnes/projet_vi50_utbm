public static class Constants
{
    
    // River Game Constants
    public static readonly string riverGameControllerName = "riverGameControllerName";
    public static readonly string ropeGameControllerName = "ropeGameControllerName";
    public static readonly string cubeLetterBaseName = "cube";
    public static readonly string boxBaseName = "box";
    public static readonly int NbOfCubes = 0;
}
