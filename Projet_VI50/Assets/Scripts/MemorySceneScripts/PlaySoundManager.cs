﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySoundManager : Singleton<PlaySoundManager>
{

	[SerializeField] private AudioClip _goodSound;
	[SerializeField] private AudioClip _badSound;

	private AudioSource _audioSource;

	private void Start()
	{
		_audioSource = GetComponent<AudioSource>();
	}

	public void PlayGoodSound()
	{
		_audioSource.clip = _goodSound;
		_audioSource.Play();
	}

	public void PlayBadSound()
	{
		_audioSource.clip = _badSound;
		_audioSource.Play();
	}
}
