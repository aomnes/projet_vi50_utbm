﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class CubeImageScript : MonoBehaviour
{
    //utilisé pour ajouter les couleurs lors de l'instantiate
    public GameObject childrenColor;
    public GameObject childrenBlock;

    [SerializeField] private bool _isVisible = true;
    [SerializeField] private bool _isDone;

    [SerializeField] private DataCouple _dataCouple;
    
    private void OnMouseDown()
    {
        ActionOneBlock();
    }

    private void OnEnable()
    {
        GamePlay.DelegateRotationEachBox += RotateY;
    }

    private void OnDisable()
    {
        GamePlay.DelegateRotationEachBox -= RotateY;
    }

    public void RotateY()
    {
        _isVisible = !_isVisible;
        this.transform.RotateAround(this.transform.position, Vector3.up, 180);
    }

    public void ActionOneBlock()
    {
        if (GamePlay.Instance.IsAlreadyStart && !_isDone && !_isVisible && !GamePlay.Instance.OneCurrentInCheck)
        {
            GamePlay.Instance.OneCurrentInCheck = true;
            RotateY();
            _dataCouple = GenerateCubeGrid.Instance.DataCouple.Where(t => (t.GameObjectA.Equals(this.gameObject)
                                                             || t.GameObjectB.Equals(this.gameObject))).ToList()[0];
            
            //1 deja visible et bonne couleur
            if (GamePlay.Instance.OneInGridVisible != null 
                     && (_dataCouple.GameObjectA == GamePlay.Instance.OneInGridVisible ||
                         _dataCouple.GameObjectB == GamePlay.Instance.OneInGridVisible)
                     )
            {
                Debug.Log("1 deja visible et bonne couleur");
                TextUIManager.Instance.UpdateScoreUi();
                PlaySoundManager.Instance.PlayGoodSound();
                
                //on passe le gameobject a "done" (comme ça on ne peux plus le clicker)
                _isDone = true;
                if (_dataCouple.GameObjectA == GamePlay.Instance.OneInGridVisible)
                {
                    _dataCouple.GameObjectA.GetComponent<CubeImageScript>()._isDone = true;
                } else if (_dataCouple.GameObjectB == GamePlay.Instance.OneInGridVisible)
                {
                    _dataCouple.GameObjectB.GetComponent<CubeImageScript>()._isDone = true;
                }
                
                GamePlay.Instance.OneInGridVisible = null;
                GamePlay.Instance.OneCurrentInCheck = false;
            }
            //1 deja visible mais pas bonne couleur
            else if (GamePlay.Instance.OneInGridVisible != null)
            {
                Debug.Log("1 deja visible mais pas bonne couleur");
                GamePlay.Instance.NumberTryFail++;
                PlaySoundManager.Instance.PlayBadSound();
                StartCoroutine(Wait());
            }
            //rien ok
            else if (GamePlay.Instance.OneInGridVisible == null)
            {
                Debug.Log("rien ok");
                GamePlay.Instance.OneInGridVisible = this.gameObject;
                GamePlay.Instance.OneCurrentInCheck = false;
            }
        }
    }

    private IEnumerator Wait()
    {
        Debug.Log("wait 1s");
        yield return new WaitForSeconds(1f);
        //rotate les 2 app attente 2 seconde
        GamePlay.Instance.OneInGridVisible.GetComponent<CubeImageScript>().RotateY();
        this.RotateY();
        GamePlay.Instance.OneInGridVisible = null;
        GamePlay.Instance.OneCurrentInCheck = false;
    }
}
