﻿using System.Collections.Generic;
using UnityEngine;

public class GenerateCubeGrid : Singleton<GenerateCubeGrid>
{

	[SerializeField] private GameObject _prefabCube;

	[SerializeField] private int _height; 
	[SerializeField] private int _width;

	[SerializeField] private float _spaceHeight;
	[SerializeField] private float _spaceWidth;

	[SerializeField] public List<DataCouple> DataCouple;

	[ContextMenu("GenerateGrid")]
	private void GenrateGrid()
	{
		List<GameObject> gridGameObject = new List<GameObject>();
		
		for(int row = 0; row < _height; row++)
		{
			for (int colomn = 0; colomn < _width; colomn++)
			{
				GameObject tempGo = Instantiate(_prefabCube, this.transform);
				Vector3 sizeCube = tempGo.transform.localScale;
				
				//changeName
				tempGo.name = "element_" + row + "_" + colomn;
				//move box
				tempGo.transform.localPosition = new Vector3(0f, colomn * (sizeCube.y + _spaceWidth), row * (sizeCube.z + _spaceHeight));
				//save box
				//row * _width + colomn
				gridGameObject.Add(tempGo);
			}
		}

		foreach (var dataCouple in DataCouple)
		{
			int tmpRandomA = (int)Random.Range(0f, gridGameObject.Count);
			GameObject goA = gridGameObject[tmpRandomA];
			dataCouple.GameObjectA = goA;
			dataCouple.GameObjectA.GetComponent<CubeImageScript>().childrenColor.GetComponent<Renderer>().material.color = dataCouple.Color;
			gridGameObject.RemoveAt(tmpRandomA);
			
			int tmpRandomB = (int)Random.Range(0f, gridGameObject.Count);
			GameObject goB = gridGameObject[tmpRandomB];
			dataCouple.GameObjectB = goB;
			dataCouple.GameObjectB.GetComponent<CubeImageScript>().childrenColor.GetComponent<Renderer>().material.color = dataCouple.Color;
			gridGameObject.RemoveAt(tmpRandomB);
		}
	}

	private void Start()
	{
		GenrateGrid();
	}
}

[System.Serializable]
public class DataCouple
{
	public Color Color;
	public GameObject GameObjectA;
	public GameObject GameObjectB;
}