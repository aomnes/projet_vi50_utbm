﻿using UnityEngine;
using UnityEngine.UI;

public class TextUIManager : Singleton<TextUIManager>
{

    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _startTipsText;
    [SerializeField] private Text _tiggerToStartText;
    [SerializeField] private Text _giveUpText;


    private void Start()
    {
        _scoreText.enabled = false;
        _giveUpText.enabled = false;
        _startTipsText.enabled = true;
        _tiggerToStartText.enabled = true;
    }

    public void StartPlaySelectColor()
    {
        _tiggerToStartText.enabled = false;
        _giveUpText.enabled = true;
        _scoreText.text = "Score :" + GamePlay.Instance.Score + "/" + GamePlay.Instance.TotalColor;
        _scoreText.enabled = true;
    }

    public void UpdateScoreUi()
    {
        GamePlay.Instance.IncrementScore();
        _scoreText.text = "Score :" + GamePlay.Instance.Score + "/" + GamePlay.Instance.TotalColor;
    }
}
