﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(TextUIManager))]
public class GamePlay : Singleton<GamePlay>
{
	//1 visible sans prendre en compte ceux validé
	[NonSerialized] public GameObject OneInGridVisible = null;
	//permet de s'assurer que l'on ne vas pas clicker sur 2 box en même temps
	[NonSerialized] public bool OneCurrentInCheck = false;

	[NonSerialized] public bool IsAlreadyStart = false;

	
	[SerializeField] private GameObject _gridCubes;
	[SerializeField] private GameObject _imageWin;
	[SerializeField] private ActivateLaserMemoryGame _activateLaserMemoryGame;
	
	public delegate void MyDelegate();
	public static MyDelegate DelegateRotationEachBox;

	[Header("GameData")]
	public int Score = 0;
	public int NumberTryFail = 0;
	public int TotalColor;
	
	private void Awake()
	{
		TotalColor = GenerateCubeGrid.Instance.DataCouple.Count;
		_imageWin.SetActive(false);
	}

	private void Start()
	{
		_activateLaserMemoryGame = this.GetComponent<ActivateLaserMemoryGame>();
	}

	public void IncrementScore()
	{
		Score++;
		if (Score == TotalColor)
		{
			Debug.Log("You win!!!");
			Destroy(_gridCubes);
			_imageWin.SetActive(true);
			StartCoroutine(WaitFinish());
		}
	}

	private void Update()
	{
		//gachette to start
		if (!IsAlreadyStart && ActionButtonTouch.IsActiveButtonStartGame() && DelegateRotationEachBox != null)
		{
			IsAlreadyStart = true;
			DelegateRotationEachBox.Invoke();
			TextUIManager.Instance.StartPlaySelectColor();
			_activateLaserMemoryGame.EnableLaser();
		}

		//abandonner en fonction touch gachette
		if (IsAlreadyStart)
		{
			ActionButtonTouch.GiveUp();
		}
	}
	
	private IEnumerator WaitFinish()
	{
		Debug.Log("wait 2s before exit");
		yield return new WaitForSeconds(4f);
		DataGame.Instance.CurrentGame.IsDone = true;
		DataGame.Instance.CurrentGame.Score = Score;
		LevelManager.LoadMain();
	}
}
