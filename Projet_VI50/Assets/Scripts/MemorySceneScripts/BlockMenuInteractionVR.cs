﻿using UnityEngine;

public class BlockMenuInteractionVR : MonoBehaviour {

	private Color _backColorIdle;
    
	private bool CheckBlock(Transform t)
	{
		return (t.gameObject.GetComponent<CubeImageScript>() != null);
	}
    
	public void OnHoverEnter(Transform t)
	{
		if (CheckBlock(t))
		{
			Renderer renderer = t.gameObject.GetComponentInChildren<ColorPlanBackBlock>().gameObject
				.GetComponent<Renderer>();

			_backColorIdle = renderer.material.color;

			//Debug.Log("OnHoverEnter" + t.gameObject.name + "-------" + t.parent.name);

			renderer.material.color = new Color(0.13f, 1f, 0f);
		}
	}

	public void OnHoverExit(Transform t) {

		if (CheckBlock(t))
		{
			Renderer renderer = t.gameObject.GetComponentInChildren<ColorPlanBackBlock>().gameObject
				.GetComponent<Renderer>();
			//Debug.Log("OnHoverExit" + t.gameObject.name + "-------" + t.parent.name);

			renderer.material.color = _backColorIdle;
		}
	}

	public void OnSelected(Transform t)
	{
		if (CheckBlock(t))
		{
			t.gameObject.GetComponent<CubeImageScript>().ActionOneBlock();
		}
	}
}
