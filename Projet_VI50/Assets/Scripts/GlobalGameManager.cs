﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(ActivateLaser))]
public class GlobalGameManager : Singleton<GlobalGameManager>
{    
    [Header("Configuration Game")]
    //declare par une autre classe
    [SerializeField] public Animator _animatorTable;

    private ActivateLaser _laserActivate;
    
    [Header("Init Game")]
    [SerializeField] private ConfigurationGroundScript _configurationGroundScript;

    [SerializeField] private GameObject _model3DCoffre;
    
    private void Start()
    {
        _model3DCoffre.SetActive(false);
        _laserActivate = GetComponent<ActivateLaser>();
        _laserActivate.DisableLaser();

        foreach (var jeu in DataGame.Instance.MiniJeux)
        {
            if (!jeu.IsDone) return;
        }
        Debug.Log("WIN !!!! \n BRAVO TOUS LES JEU SONT FAIT");
        _model3DCoffre.SetActive(true);
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadLevel(int id)
    {
        SceneManager.LoadScene(id);
    }

    public void ActionOnValidateButtonConfiguration()
    {
        Debug.Log(string.Format("Configuration OK : {0} : {1}", DataGame.Instance.DataPlayer.NameClasseStudent, DataGame.Instance.DataPlayer.NameMascotSelected));
        StartCoroutine(RunAnimation());
    }
    
    private IEnumerator RunAnimation()
    {
        _animatorTable.SetBool("translation", true);
        yield return new WaitForSeconds(2f);
        DataGame.Instance.IsConfigurationDone = true;
        Destroy(_animatorTable.gameObject);
        _laserActivate.EnableLaser();
        //affichage des sphere menu
        _configurationGroundScript.InitiateMenuSphereSelectGame();
    }
}