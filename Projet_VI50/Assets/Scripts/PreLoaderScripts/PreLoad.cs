﻿using System.Collections;
using UnityEngine;

public class PreLoad : MonoBehaviour {
	
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(1f);
		LevelManager.LoadMain();
	}
}
