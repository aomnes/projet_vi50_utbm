﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class RopeCollision : MonoBehaviour
{
    [SerializeField] private AudioSource _cubeBoxCollisionSound;
    private RopeGameController _gameController;

    void Start()
    {
        GameObject ropeGameController = GameObject.Find(Constants.ropeGameControllerName);
        _gameController = (RopeGameController)ropeGameController.GetComponent(typeof(RopeGameController));
    }

    void OnCollisionEnter(Collision collision)
    {
        _cubeBoxCollisionSound.Play(); // play sound
        // Object collisionné
        MeshRenderer mr = collision.collider.GetComponent<MeshRenderer>();
        // parent de l'object qui collisioné
        MeshRenderer mrr = this.gameObject.transform.parent.GetComponentInParent<MeshRenderer>();
        if (this.gameObject.transform.parent.name == collision.transform.name) // si les éléments relié correspondent
        {
            mr.material.color = Color.green;
            mrr.material.color = Color.green;
            _gameController.AddIndex();
            if (RopeGameController._RightAnswer == _gameController._numberOfElements) // si on bien relié tout les cubes entre eux
            {
                _gameController.PauseGame();
            }
        }
        else // si l'association est fausse on affiche les cubes en rouges
        {
            _gameController.AddError();
            mr.material.color = Color.red;
            mrr.material.color = Color.red;
        }

    }

    void OnCollisionStay(Collision collision)
    {
        MeshRenderer mr = collision.collider.GetComponent<MeshRenderer>();
        MeshRenderer mrr = this.gameObject.transform.parent.GetComponentInParent<MeshRenderer>();
        if (this.gameObject.transform.parent.name == collision.transform.name)
        {
            mr.material.color = Color.green;
            mrr.material.color = Color.green;
        }
        else
        {
            mr.material.color = Color.red;
            mrr.material.color = Color.red;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (this.gameObject.transform.parent.name == collision.transform.name)
        {
            _gameController.ReduceIndex();
        }
        MeshRenderer mr = collision.collider.GetComponent<MeshRenderer>();
        MeshRenderer mrr = this.gameObject.transform.parent.GetComponentInParent<MeshRenderer>();
        mr.material.color = Color.white;
        mrr.material.color = Color.white;
    }
}
