﻿using System.Collections.Generic;
using UnityEngine;

public class CubeSpawnerRope : MonoBehaviour {

    [SerializeField] private int _textFontSize;
    [SerializeField] private float _initialZ;
    [SerializeField] private float _spaceBetweenCubes;
    [SerializeField] private float _initialHeight;
    [SerializeField] private float _heightDifference;

    internal void CubesToSpawnFormWordGameplay(List<string> gameElementList, List<Sprite> spriteList)
    {
        for (int i = 0; i < gameElementList.Count; i++)
        {
            Vector3 position = new Vector3();
            position.x = 0;
            position.y = _initialHeight;
            position.z = _initialZ + ((0.25f + 0.02f) * i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPool(
                "TextCube",
                position,
                Quaternion.identity);
            cube.name = gameElementList[i];
            SetupTextMesh(cube, cube.name);
        }
        gameElementList.ShuffleRopeGameData(spriteList);
        for (int i = 0; i < gameElementList.Count; i++)
        {
            Vector3 position = new Vector3();
            position.x = _spaceBetweenCubes;
            position.y = _initialHeight + _heightDifference;
            position.z = _initialZ + ((0.25f + 0.02f) * i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPoolWithRope(
                "SpriteCubeWithRope",
                position,
                Quaternion.identity);
            cube.name = gameElementList[i];
            SetupSpriteRenderer(cube, cube.name, spriteList[i]);
        }
    }

    internal void CubesTospawnMathGameplay(List<string> gameElementQuestionList, List<string> gameElementAnswerList)
    {
        // Questions
        for (int i = 0; i < gameElementQuestionList.Count; i++)
        {Vector3 position = new Vector3();
            position.x = 0;
            position.y = _initialHeight;
            position.z = _initialZ + ((0.25f + 0.02f)*i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPool(
                "TextCube",
                position,
                Quaternion.identity);
            cube.name = gameElementAnswerList[i];
            SetupTextMesh(cube, cube.name);
        }
        // Answers
        gameElementQuestionList.ShuffleRopeGameData(gameElementAnswerList);
        for (int i = 0; i < gameElementAnswerList.Count; i++)
        {
            Vector3 position = new Vector3();
            position.x = _spaceBetweenCubes;
            position.y = _initialHeight + _heightDifference;
            position.z = _initialZ + ((0.25f + 0.02f) * i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPoolWithRope(
                "TextCubeWithRope",
                position,
                Quaternion.identity);
            cube.name = gameElementQuestionList[i];
            SetupTextMeshForMathGameplay(cube, cube.name, gameElementAnswerList[i]);
        }
    }

    internal void CubesToSpawnFormGameplay(List<string> gameElementList, List<Sprite> spriteList)
    {
        for (int i = 0; i < spriteList.Count; i++)
        {
            Vector3 position = new Vector3();
            position.x = 0;
            position.y = _initialHeight;
            position.z = _initialZ + ((0.25f + 0.02f) * i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPool(
                "SpriteCube",
                position,
                Quaternion.identity);
            cube.name = gameElementList[i];
            SetupSpriteRenderer(cube, cube.name, spriteList[i]);
        }
        spriteList.ShuffleRopeGameData(gameElementList);
        for (int i = 0; i < spriteList.Count; i++)
        {
            Vector3 position = new Vector3();
            position.x = _spaceBetweenCubes;
            position.y = _initialHeight + _heightDifference;
            position.z = _initialZ + ((0.25f + 0.02f) * i);
            GameObject cube = ObjectPoolerRope.Instance.SpawnFromPool(
                "SpriteCubeWithRope",
                position,
                Quaternion.identity);
            cube.name = gameElementList[i];
            SetupSpriteRenderer(cube, cube.name, spriteList[i]);
        }
    }

    // setup cubes with texts
    private void SetupTextMesh(GameObject cube, string _text)
    {
        TextMesh t = cube.GetComponentInChildren<TextMesh>();
        t.text = _text;
        t.fontSize = _textFontSize;
        t.color = Color.black;
        t.transform.position = new Vector3(
            cube.transform.position.x - (cube.transform.localScale.x / 2),
            cube.transform.position.y,
            cube.transform.position.z
        );
        Vector3 eulerAngles = t.transform.rotation.eulerAngles;
        eulerAngles.y = 90;
        t.transform.eulerAngles = eulerAngles;
        t.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    // setup cubes with images
    private void SetupSpriteRenderer(GameObject cube, string _name, Sprite _sprite)
    {
        SpriteRenderer s = cube.GetComponentInChildren<SpriteRenderer>();
        s.sprite = _sprite;
        s.color = Color.white;
        s.transform.position = new Vector3(
            cube.transform.position.x - (cube.transform.localScale.x / 2) - 0.01f,
            cube.transform.position.y,
            cube.transform.position.z
        );
        Vector3 eulerAngles = s.transform.rotation.eulerAngles;
        eulerAngles.y = 90;
        s.transform.eulerAngles = eulerAngles;
        // resize depending on the initial image size
        // Display the image with the maximum height, keeping the correct pixel dimension
        float recrop = 7.5f / _sprite.bounds.size.y;
        s.transform.localScale = new Vector3(0.1f*recrop, 0.1f*recrop, 0.1f);
    }

    // setup cubes with text
    private void SetupTextMeshForMathGameplay(GameObject cube, string _text, string cubename)
    {
        TextMesh t = cube.GetComponentInChildren<TextMesh>();
        t.text = _text;
        t.fontSize = _textFontSize;
        t.color = Color.black;
        t.transform.position = new Vector3(
            cube.transform.position.x - (cube.transform.localScale.x / 2),
            cube.transform.position.y,
            cube.transform.position.z
        );
        t.GetComponentInParent<Transform>().parent.name = cubename;
        Vector3 eulerAngles = t.transform.rotation.eulerAngles;
        eulerAngles.y = 90;
        t.transform.eulerAngles = eulerAngles;
        t.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

}