using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RopeGameController : MonoBehaviour
{
    #region Serialize Fields
    [SerializeField] private DataGameRope _dataGameRope;
    [SerializeField] private int _numberOfTurns;
    [SerializeField] public int _numberOfElements;
    [SerializeField] private Canvas _textCanvas;
    [SerializeField] private Text _instructionsText;
    [SerializeField] private GameObject _scoreDisplay;
    [SerializeField] private AudioSource _nextLevel;
    #endregion

    #region Private fields
    private List<Sprite> __spritePath;
    private List<string> _gameElementPath;
    private List<string> _gameElementPathAnswer;
    private GameType _gameType;
    private SchoolLevels _schoolLevels;
    private int _turnIndex = 0;
    private bool _startGame;
    private int _score = 0;
    #endregion

    #region Public fields
    public static int _RightAnswer = 0;
    public static int _FalseAnswer = 0;
    #endregion

    #region Unity fonctions
    private void Start()
    {
        name = Constants.ropeGameControllerName;
        SetDataGameInformation();
        ShowInstructions();
    }
    
    private void Update()
    {
        if (!_startGame && ActionButtonTouch.IsActiveButtonStartGame())
        {
            _textCanvas.gameObject.SetActive(false);
            switch (_gameType)
            {
                case GameType.FormWord:
                    SetupFormWordGameplay(_schoolLevels);
                    _startGame = true;
                    break;
                case GameType.Math:
                    SetupMathGameplay(_schoolLevels);
                    _startGame = true;
                    break;
                case GameType.Form:
                    SetupFormGameplay(_schoolLevels);
                    _startGame = true;
                    break;
                default:
                    throw new Exception("Ce type de jeu n'existe pas");
            }
        }

        if (_startGame && (OVRInput.Get(OVRInput.RawButton.A) && OVRInput.Get(OVRInput.RawButton.X))
            || (OVRInput.Get(OVRInput.RawButton.B) && OVRInput.Get(OVRInput.RawButton.Y))
            || Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.E)) // Leave the game
        {
            leaveGame();
        }


    }
    #endregion

    #region Public fonctions
    public void AddIndex() // incr�mente l'index de bonnes r�ponses
    {
        _RightAnswer++;
    }

    public void AddError() // incr�mente l'index de mauvaises r�ponses
    {
        _FalseAnswer++;
    }

    public void ReduceIndex() // r�duit l'index de bonnes r�ponses
    {
        _RightAnswer--;
    }

    public void ResetIndex()// reset l'index de bonnes r�ponses
    {
        _RightAnswer = 0;
    }

    public void ResetError() // reset l'index de mauvaises r�ponses
    {
        _FalseAnswer = 0;
    }

    public void NextTurn() // Lance le prochain niveau
    {
        UpdateScore();
        UpdateScoreDisplay();
        _turnIndex++;
        if (_turnIndex >= _numberOfTurns) // si on a termin� tout les niveaux
        {
            EndGame();
            return;
        }
        ResetIndex();
        ResetError();
        removeCubes();
        switch (_gameType) // setup des prochains cubes selon le gametype & classe
        {
            case GameType.FormWord:
                SetupFormWordGameplay(_schoolLevels);
                break;
            case GameType.Math:
                SetupMathGameplay(_schoolLevels);
                break;
            case GameType.Form:
                SetupFormGameplay(_schoolLevels);
                break;
            default:
                throw new Exception("Ce type de jeu n'existe pas");
        }
    }

    public void PauseGame()
    {
        _nextLevel.Play();
        StartCoroutine(PauseForSeconds(2));
    }
    #endregion

    #region Private fonctions

    private void UpdateScoreDisplay()
    {
        TextMesh scoreText = _scoreDisplay.GetComponent<TextMesh>();
        scoreText.text = "Score : " + _score + " %";
    }

    private void UpdateScore()
    {
        float currentscore = (Math.Max(((_numberOfElements - (_FalseAnswer * 0.5f)) / _numberOfElements), 0));
        currentscore *= 100;
        _score += (int) currentscore;
        if (_turnIndex == 0)
        {
            _score = _score / (_turnIndex + 1);
        }
        else
        {
            _score = _score / 2;
        }
    }

    private void removeCubes()
    {
        List<GameObject> gameobjects = GameObject.FindGameObjectsWithTag("Rope").ToList();
        foreach (var obj in gameobjects) // on d�truit les anciens cubes
        {
            Destroy(obj);
        }
    }

    private void leaveGame()
    {
        removeCubes();
        _instructionsText.text = Instructions.leaveGame;
        _textCanvas.gameObject.SetActive(true);
        ExitGame();
    }

    private void EndGame()
    {
        removeCubes();
        if (DataGame.Instance != null)
        {
            DataGame.Instance.CurrentGame.IsDone = true;
            DataGame.Instance.CurrentGame.Score = (int) _score;
        }
        _instructionsText.text = Instructions.endGame;
        _textCanvas.gameObject.SetActive(true);
        ExitGame();
    }

    IEnumerator PauseForSeconds(int _seconds)
    {
        yield return new WaitForSeconds(_seconds);
        NextTurn();
        yield break;
    }

    private void ExitGame()
    {
        StartCoroutine(WaitForSeconds(5));
    }

    IEnumerator WaitForSeconds(int _seconds)
    {
        yield return new WaitForSeconds(_seconds);
        LevelManager.LoadMain();
        yield break;
    }

    private void SetDataGameInformation()
    {
        // Si on ne trouve pas d'instance de datagame
        // on d�fini un gameplay et une classe par d�fault
        if (DataGame.Instance == null)
        {
            _gameType = GameType.FormWord;
            _schoolLevels = SchoolLevels.CM1;
            return;
        }
        MiniJeuScriptableObject game = DataGame.Instance.CurrentGame;
        _gameType = game.GameType;
        _schoolLevels = DataGame.Instance.DataPlayer.NameClasseStudent;
    }

    private void ShowInstructions()
    {
        switch (_gameType)
        {
            case GameType.FormWord:
                _instructionsText.text = Instructions.ropeFormWordInstructions;
                break;
            case GameType.Math:
                _instructionsText.text = Instructions.ropeMathInstructions;
                break;
            case GameType.Form:
                _instructionsText.text = Instructions.ropeFormInstructions;
                break;
            default:
                throw new Exception("Ce type de jeu n'existe pas");
        }
    }

    private void SetupFormWordGameplay(SchoolLevels _schoolLevels)
    {

        _gameElementPath = new List<string>();
        __spritePath = new List<Sprite>();
        switch (_schoolLevels)
        {
            case SchoolLevels.CP:
                _gameElementPath.Add(_dataGameRope.FormWordDataListCP[_turnIndex * _numberOfElements].Word);
                __spritePath.Add(_dataGameRope.FormWordDataListCP[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.FormWordDataListCP[(_turnIndex * _numberOfElements) + i].Word);
                    __spritePath.Add(_dataGameRope.FormWordDataListCP[(_turnIndex * _numberOfElements) + i].Image);
                }
                break;

            case SchoolLevels.CE1:
                _gameElementPath.Add(_dataGameRope.FormWordDataListCE1[_turnIndex * _numberOfElements].Word);
                __spritePath.Add(_dataGameRope.FormWordDataListCE1[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.FormWordDataListCE1[(_turnIndex * _numberOfElements) + i].Word);
                    __spritePath.Add(_dataGameRope.FormWordDataListCE1[(_turnIndex * _numberOfElements) + i].Image);
                }
                break;

            case SchoolLevels.CE2:
                _gameElementPath.Add(_dataGameRope.FormWordDataListCE2[_turnIndex * _numberOfElements].Word);
                __spritePath.Add(_dataGameRope.FormWordDataListCE2[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.FormWordDataListCE2[(_turnIndex * _numberOfElements) + i].Word);
                    __spritePath.Add(_dataGameRope.FormWordDataListCE2[(_turnIndex * _numberOfElements) + i].Image);
                }
                break;

            case SchoolLevels.CM1:
                _gameElementPath.Add(_dataGameRope.FormWordDataListCM1[_turnIndex * _numberOfElements].Word);
                __spritePath.Add(_dataGameRope.FormWordDataListCM1[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.FormWordDataListCM1[(_turnIndex * _numberOfElements) + i].Word);
                    __spritePath.Add(_dataGameRope.FormWordDataListCM1[(_turnIndex * _numberOfElements) + i].Image);
                }
                break;

            case SchoolLevels.CM2:
                _gameElementPath.Add(_dataGameRope.FormWordDataListCM2[_turnIndex * _numberOfElements].Word);
                __spritePath.Add(_dataGameRope.FormWordDataListCM2[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.FormWordDataListCM2[(_turnIndex * _numberOfElements) + i].Word);
                    __spritePath.Add(_dataGameRope.FormWordDataListCM2[(_turnIndex * _numberOfElements) + i].Image);
                }
                break;
        }
        GameObject cubeSpawner = GameObject.Find("CubeSpawner");
        CubeSpawnerRope _cubeSpawner = (CubeSpawnerRope)cubeSpawner.GetComponent(typeof(CubeSpawnerRope));
        _cubeSpawner.CubesToSpawnFormWordGameplay(_gameElementPath, __spritePath);
    }

    
    private void SetupMathGameplay(SchoolLevels _schoolLevels)
    {
        _gameElementPath = new List<string>();
        _gameElementPathAnswer = new List<string>();

        switch (_schoolLevels)
        {
            case SchoolLevels.CP:
                _gameElementPath.Add(_dataGameRope.MathListCP[_turnIndex * _numberOfElements].Operation);
                _gameElementPathAnswer.Add(_dataGameRope.MathListCP[_turnIndex * _numberOfElements].Result);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.MathListCP[(_turnIndex * _numberOfElements) + i].Operation);
                    _gameElementPathAnswer.Add(_dataGameRope.MathListCP[(_turnIndex * _numberOfElements) + i].Result);
                }
                break;

            case SchoolLevels.CE1:
                _gameElementPath.Add(_dataGameRope.MathListCE1[_turnIndex * _numberOfElements].Operation);
                _gameElementPathAnswer.Add(_dataGameRope.MathListCE1[_turnIndex * _numberOfElements].Result);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.MathListCE1[(_turnIndex * _numberOfElements) + i].Operation);
                    _gameElementPathAnswer.Add(_dataGameRope.MathListCE1[(_turnIndex * _numberOfElements) + i].Result);
                }
                break;

            case SchoolLevels.CE2:
                _gameElementPath.Add(_dataGameRope.MathListCE2[_turnIndex * _numberOfElements].Operation);
                _gameElementPathAnswer.Add(_dataGameRope.MathListCE2[_turnIndex * _numberOfElements].Result);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.MathListCE2[(_turnIndex * _numberOfElements) + i].Operation);
                    _gameElementPathAnswer.Add(_dataGameRope.MathListCE2[(_turnIndex * _numberOfElements) + i].Result);
                }
                break;

            case SchoolLevels.CM1:
                _gameElementPath.Add(_dataGameRope.MathListCM1[_turnIndex * _numberOfElements].Operation);
                _gameElementPathAnswer.Add(_dataGameRope.MathListCM1[_turnIndex * _numberOfElements].Result);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.MathListCM1[(_turnIndex * _numberOfElements) + i].Operation);
                    _gameElementPathAnswer.Add(_dataGameRope.MathListCM1[(_turnIndex * _numberOfElements) + i].Result);
                }
                break;

            case SchoolLevels.CM2:
                _gameElementPath.Add(_dataGameRope.MathListCM2[_turnIndex * _numberOfElements].Operation);
                _gameElementPathAnswer.Add(_dataGameRope.MathListCM2[_turnIndex * _numberOfElements].Result);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.MathListCM2[(_turnIndex * _numberOfElements) + i].Operation);
                    _gameElementPathAnswer.Add(_dataGameRope.MathListCM2[(_turnIndex * _numberOfElements) + i].Result);
                }
                break;
        }
        GameObject cubeSpawner = GameObject.Find("CubeSpawner");
        CubeSpawnerRope _cubeSpawner = (CubeSpawnerRope)cubeSpawner.GetComponent(typeof(CubeSpawnerRope));
        _cubeSpawner.CubesTospawnMathGameplay(_gameElementPath, _gameElementPathAnswer);

    }
    
    private void SetupFormGameplay(SchoolLevels _schoolLevels)
    {
        _gameElementPath = new List<string>();
        __spritePath = new List<Sprite>();

        switch (_schoolLevels)
        {
            case SchoolLevels.CP:
                _gameElementPath.Add(_dataGameRope.SpriteListCP[_turnIndex * _numberOfElements].name);
                __spritePath.Add(_dataGameRope.SpriteListCP[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.SpriteListCP[(_numberOfElements * _turnIndex) + i].name);
                    __spritePath.Add(_dataGameRope.SpriteListCP[(_numberOfElements * _turnIndex) + i].Image);
                }
                break;

            case SchoolLevels.CE1:
                _gameElementPath.Add(_dataGameRope.SpriteListCE1[_turnIndex * _numberOfElements].name);
                __spritePath.Add(_dataGameRope.SpriteListCE1[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.SpriteListCE1[(_numberOfElements * _turnIndex) + i].name);
                    __spritePath.Add(_dataGameRope.SpriteListCE1[(_numberOfElements * _turnIndex) + i].Image);
                }
                break;

            case SchoolLevels.CE2:
                _gameElementPath.Add(_dataGameRope.SpriteListCE2[_turnIndex * _numberOfElements].name);
                __spritePath.Add(_dataGameRope.SpriteListCE2[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.SpriteListCE2[(_numberOfElements * _turnIndex) + i].name);
                    __spritePath.Add(_dataGameRope.SpriteListCE2[(_numberOfElements * _turnIndex) + i].Image);
                }
                break;

            case SchoolLevels.CM1:
                _gameElementPath.Add(_dataGameRope.SpriteListCM1[_turnIndex * _numberOfElements].name);
                __spritePath.Add(_dataGameRope.SpriteListCM1[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.SpriteListCM1[(_numberOfElements * _turnIndex) + i].name);
                    __spritePath.Add(_dataGameRope.SpriteListCM1[(_numberOfElements * _turnIndex) + i].Image);
                }
                break;

            case SchoolLevels.CM2:
                _gameElementPath.Add(_dataGameRope.SpriteListCM2[_turnIndex * _numberOfElements].name);
                __spritePath.Add(_dataGameRope.SpriteListCM2[_turnIndex * _numberOfElements].Image);
                for (int i = 1; i < _numberOfElements; i++)
                {
                    _gameElementPath.Add(_dataGameRope.SpriteListCM2[(_numberOfElements * _turnIndex) + i].name);
                    __spritePath.Add(_dataGameRope.SpriteListCM2[(_numberOfElements * _turnIndex) + i].Image);
                }
                break;
        }
        GameObject cubeSpawner = GameObject.Find("CubeSpawner");
        CubeSpawnerRope _cubeSpawner = (CubeSpawnerRope)cubeSpawner.GetComponent(typeof(CubeSpawnerRope));
        _cubeSpawner.CubesToSpawnFormGameplay(_gameElementPath, __spritePath);
    }
#endregion

}