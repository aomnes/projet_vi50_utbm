﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolerRope : MonoBehaviour {

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    # region Singleton

    public static ObjectPoolerRope Instance;

    private void Awake()
    {
        Instance = this;
    }

    # endregion

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " does not exists");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }

    public GameObject SpawnFromPoolWithRope(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning("Pool with tag " + tag + " does not exists");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

/*        Debug.Log("spawn from pool rotation : " + rotation);
        Debug.Log("spawn from pool position : " + position);*/

        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        
        Transform[] Elements = objectToSpawn.GetComponentsInChildren<Transform>();
        foreach (var e in Elements)
        {
            if (e.gameObject.tag=="Rope")
            {
                e.position = new Vector3(objectToSpawn.transform.position.x,e.transform.position.y, objectToSpawn.transform.position.z);
            }
        }

        poolDictionary[tag].Enqueue(objectToSpawn);

        return objectToSpawn;
    }

}
