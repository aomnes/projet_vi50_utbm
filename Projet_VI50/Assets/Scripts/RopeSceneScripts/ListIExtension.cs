using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListIExtension
{
    // Shuffles the element order of the list
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
    public static void ShuffleRopeGameData<T1, T2>(this IList<T1> ts, IList<T2> ts2)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            var tmp2 = ts2[i];
            ts[i] = ts[r];
            ts2[i] = ts2[r];
            ts[r] = tmp;
            ts2[r] = tmp2;
        }
    }
}
