﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSpawner : MonoBehaviour
{
    private LineRenderer lineRenderer;

    // les deux extrémités de la corde
    public Transform origin;
    public Transform destination;

	// Use this for initialization
	void Start ()
	{
        // on crée une corde entre la position de nos deux extrémités
	    lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0,origin.position);
        lineRenderer.SetPosition(1,destination.position);
	}
	
	// Modifie si necessaire la position de la corde lorsque ces extrémités sont déplacées
	void Update () {
	    lineRenderer.SetPosition(0, origin.position);
	    lineRenderer.SetPosition(1, destination.position);
    }
}

