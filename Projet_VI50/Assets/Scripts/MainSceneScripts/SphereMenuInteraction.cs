﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SphereMenuInteraction : MonoBehaviour
{

    private Color _backColorIdle;
    
    private bool CheckSphere(Transform t)
    {
        return (t.gameObject.GetComponent<SphereSelectGame>() != null);
    }
    
    public void OnHoverEnter(Transform t)
    {
        if (CheckSphere(t) && !IsGameAlreadyDone(t))
        {
            _backColorIdle = t.gameObject.GetComponent<Renderer>().material.color;

            //Debug.Log("OnHoverEnter" + t.gameObject.name + "-------" + t.parent.name);

            t.gameObject.GetComponent<Renderer>().material.color = new Color(0.11f, 0.7f, 1f, 0.1f);
        }
    }

    public void OnHoverExit(Transform t) {
        if (CheckSphere(t) && !IsGameAlreadyDone(t))
        {
            //Debug.Log("OnHoverExit" + t.gameObject.name + "-------" + t.parent.name);

            t.gameObject.GetComponent<Renderer>().material.color = _backColorIdle;
        }
    }

    public void OnSelected(Transform t)
    {
        if (CheckSphere(t) && !IsGameAlreadyDone(t))
        {
            Debug.Log("OnSelected" + t.gameObject.name + "-------" + t.parent.name);

            Debug.Log("Start Scene :" + t.gameObject.name);
            PrefabSphereScript tmp = t.gameObject.transform.parent.GetComponent<PrefabSphereScript>();
            if (tmp != null)
            {
                DataGame.Instance.CurrentGame = tmp.GameObjectSphere;
                SceneManager.LoadScene((int)tmp.GameObjectSphere.NameScene , LoadSceneMode.Single);
            }            else
                Debug.Log(" t.gameObject.transform.parent.GetComponent<PrefabSphereScript>(); == null");
        }
    }

    private bool IsGameAlreadyDone(Transform t)
    {
        PrefabSphereScript tmp = t.gameObject.transform.parent.GetComponent<PrefabSphereScript>();
        return (tmp.GameObjectSphere.IsDone);
    }
}
