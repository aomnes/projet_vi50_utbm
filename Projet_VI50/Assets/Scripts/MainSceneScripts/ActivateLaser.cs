﻿using ControllerSelection;
using UnityEngine;

public class ActivateLaser : MonoBehaviour
{
    [SerializeField]
    private OVRPointerVisualizer _ovrPointerVisualizer = null;
    [SerializeField]
    private OVRRawRaycaster _ovrRawRaycaster = null;

    private void Start()
    {
        if (_ovrPointerVisualizer == null || _ovrRawRaycaster == null)
        {
            Debug.LogWarning("GameObject ActivateLaser mal configuré !");
        }

        if (DataGame.Instance.IsConfigurationDone)
        {
            EnableLaser();
        }
        else
        {
            DisableLaser();
        }
    }

    [ContextMenu("Enable Laser")]
    public void EnableLaser()
    {
        _ovrPointerVisualizer.gameObject.SetActive(true);
        _ovrRawRaycaster.gameObject.SetActive(true);
    }
    [ContextMenu("Disable Laser")]
    public void DisableLaser()
    {
        _ovrPointerVisualizer.gameObject.SetActive(false);
        _ovrRawRaycaster.gameObject.SetActive(false);
    }
}
