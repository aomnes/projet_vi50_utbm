﻿using UnityEngine;

public class ConfigurationGroundScript : MonoBehaviour {

	[SerializeField] private GameObject _prefabSubject3D;


	private void Start() {
		if (DataGame.Instance.IsConfigurationDone)
		{
			InitiateMenuSphereSelectGame();
		}
	}

	public void InitiateMenuSphereSelectGame()
	{
		int numberMiniJeu = DataGame.Instance.MiniJeux.Length;
		float angleRotation;
		//Temporary Empty GameObject in order to use it as center when rotating tmpPrefab
		GameObject tmpCenterRotation = new GameObject("TmpCenterRotation");

		tmpCenterRotation.transform.SetParent(this.transform);
		Debug.Log("numberSuject: " + numberMiniJeu);
		
		for (var i = 0; i < numberMiniJeu; i++) {
			GameObject tmpPrefab = Instantiate(_prefabSubject3D, tmpCenterRotation.transform);
			
			//change name instance
			tmpPrefab.name = DataGame.Instance.MiniJeux[i].name;
			tmpPrefab.GetComponent<PrefabSphereScript>().GameObjectSphere = DataGame.Instance.MiniJeux[i];
			angleRotation = i * 360.0f / numberMiniJeu;
			tmpCenterRotation.transform.localRotation = Quaternion.Euler(0f, angleRotation, 0f);
			tmpPrefab.transform.SetParent(this.transform);
		}
		Destroy(tmpCenterRotation);
	}
}
