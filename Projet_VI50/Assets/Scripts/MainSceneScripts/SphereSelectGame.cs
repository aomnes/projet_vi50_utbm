﻿using UnityEngine;

public class SphereSelectGame : MonoBehaviour
{

    private Material _imageGameMaterial;
    private Material _material;
    private PrefabSphereScript _prefabSphereScript;

    private void Start()
    {
        _prefabSphereScript = GetComponentInParent<PrefabSphereScript>();
        _imageGameMaterial = _prefabSphereScript.GameObjectSphere.ImageGameMaterial;
        _material = GetComponent<Renderer>().material;

   
        if (_prefabSphereScript.GameObjectSphere.IsDone == true)
        {
            _material.color = new Color(0.15f, 1f, 0.37f, 0.95f);
            
        }
        else
        {
            this.GetComponent<Renderer>().sharedMaterial = _imageGameMaterial;
        }
    }
}
