using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
public class ButtonValidateConfiguration : MonoBehaviour {

    [SerializeField] private SoundButtonPlayer soundButton;
    [SerializeField] private BoxMascotConfiguration _boxMascotConfiguration;

    
    private AudioSource sourceAudioComponent;
    private readonly Vector3 movePositionButton = new Vector3(0f, 0.01f, 0f);

    private bool _isButtonDown = false;
    
    
    private bool _isOkValidate = false;

    public bool IsOkValidate
    {
        get { return _isOkValidate; }
    }


    public static readonly Color COLOR_ACTIVE = new Color(0.86f, 1f, 0f);
    public static readonly Color COLOR_DEFAULT = new Color(0.1f, 0.93f, 1f);
    public static readonly Color COLOR_ENABLE = new Color(0.3f, 1f, 0.07f);

    private Renderer _rendererComponent;

    private void Start()
    {
        sourceAudioComponent = GetComponent<AudioSource>();
        AddTextCube(this.gameObject.name);
        _rendererComponent = this.gameObject.GetComponent<Renderer>();

        _rendererComponent.material.color = COLOR_DEFAULT;
    }

    private void AddTextCube(string textCube)
    {
        GameObject tmpGameObjectText = new GameObject();
        
        tmpGameObjectText.transform.SetParent(this.gameObject.transform);
        tmpGameObjectText.gameObject.name = this.gameObject.name + "Text";
        
        TextMesh textMesh = tmpGameObjectText.AddComponent<TextMesh>();
        textMesh.text = textCube;
        textMesh.color = Color.black;
        textMesh.anchor = TextAnchor.MiddleCenter;
        textMesh.alignment = TextAlignment.Center;
        textMesh.fontSize = 50;
        textMesh.transform.localScale = new Vector3(0.05f, 0.1f, 1f);
        textMesh.transform.Rotate(90f, 0f, 0f);
        textMesh.transform.localPosition = new Vector3(0f, 0.5f, 0f);
    }

    private void SetColorEnabled(bool On)
    {
        if (On)
        {
            _isOkValidate = true;
            _rendererComponent.material.color = COLOR_ENABLE;
        }
        else
        {
            _isOkValidate = false;
            _rendererComponent.material.color = COLOR_DEFAULT;
        }
    }

    private void OnMouseDown()
    {
        if (_isOkValidate)
        {
            soundButton.Play(sourceAudioComponent);
            ActionButtonDown();
        }
    }
    
    private void ActionButtonDown()
    {
        ButtonDown();
        GlobalGameManager.Instance.ActionOnValidateButtonConfiguration();
    }

    
    
    public void ButtonUp()
    {
        _isButtonDown = false;
        _rendererComponent.material.color = COLOR_DEFAULT;
        this.gameObject.transform.position = this.gameObject.transform.position + movePositionButton;
    }

    public void ButtonDown()
    {
        _isButtonDown = true;
        _rendererComponent.material.color = COLOR_ACTIVE;
        this.gameObject.transform.position = this.gameObject.transform.position - movePositionButton;
    }
    
    
    
    
    
    
    
    
    
    public void UpdateBoxMascoteIn()
    {
        if (_boxMascotConfiguration.TriggerList.Count == 0)
        {
            if (DataGame.Instance.DataPlayer.NameClasseStudent != SchoolLevels.None &&
                !string.IsNullOrEmpty(DataGame.Instance.DataPlayer.NameMascotSelected))
            {
                SetColorEnabled(true);
            }
        }
    }
    
    public void UpdateBoxMascoteOut()
    {
        if (_boxMascotConfiguration.TriggerList.Count != 0)
        {
            SetColorEnabled(false);
        }
    }

    public void UpdateButtonClasseUp()
    {
        SetColorEnabled(false);
    }
    
    public void UpdateButtonClasseDown()
    {
        if (DataGame.Instance.DataPlayer.NameClasseStudent != SchoolLevels.None &&
            !string.IsNullOrEmpty(DataGame.Instance.DataPlayer.NameMascotSelected))
        {
            SetColorEnabled(true);
        }
    }
    
    
    
    
    
    
    private void OnTriggerEnter(Collider other)
    {
        OVRGrabber go = other.gameObject.GetComponent<OVRGrabber>() ??
                        other.gameObject.GetComponentInParent<OVRGrabber>() ??
                        other.gameObject.transform.parent.GetComponentInParent<OVRGrabber>();
        if (go != null)
        {
            //Debug.Log("CLICK Validate");
            OnMouseDown();
        }
    }
}
