﻿using System;
using UnityEngine;

public class InitTableConfigOnLoad : MonoBehaviour
{

	[SerializeField] private GameObject _tablePrefab;
	
	private void Start()
	{
		if (_tablePrefab == null)
		{
			Debug.LogWarning("Vous avez oublier le prefab table");
		}
		if (!DataGame.Instance.IsConfigurationDone)
		{
			Instantiate(_tablePrefab);
		}
	}
}
