﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(AudioSource))]
public class ButtonClasseConfiguration : MonoBehaviour {

    [SerializeField] private SoundButtonPlayer _soundButton;
    [SerializeField] private ButtonValidateConfiguration _buttonValidateConfiguration;
    
    private AudioSource _sourceAudioComponent;

    private readonly Vector3 _movePositionButton = new Vector3(0f, 0.01f, 0f);

    private bool _isButtonDown = false;

    public bool IsButtonDown
    {
        get { return _isButtonDown; }
    }

    public static readonly Color COLOR_ACTIVE = new Color(0.05f, 1f, 0.13f);
    public static readonly Color COLOR_DEFAULT = new Color(0.1f, 0.93f, 1f);

    private Renderer _rendererComponent;

    private void Start()
    {
        _sourceAudioComponent = GetComponent<AudioSource>();
        AddTextButtonClasse(this.gameObject.name);
        _rendererComponent = this.gameObject.GetComponent<Renderer>();

        _rendererComponent.material.color = COLOR_DEFAULT;
    }

    private void OnMouseDown()
    {
        _soundButton.Play(_sourceAudioComponent);
        //bouton non sélectionné
        if (!_isButtonDown)
        {
            SchoolLevels currentClasse = DataGame.Instance.DataPlayer.NameClasseStudent;
            //premiere action sur les boutons
            if (currentClasse == SchoolLevels.None)
            {
                DataGame.Instance.DataPlayer.NameClasseStudent = (SchoolLevels)System.Enum.Parse(typeof(SchoolLevels), this.gameObject.name);
                ActionButtonDown();
            }
            //un bouton est déjà selectionné
            else
            {
                //deselectionner le bouton déjà selectionné
                ButtonClasseConfiguration cacheCurrentClasseGo = GameObject.Find(currentClasse.ToString()).gameObject.GetComponent<ButtonClasseConfiguration>();
                cacheCurrentClasseGo.ButtonUp();
                //sélectionner le bouton enclenché.
                DataGame.Instance.DataPlayer.NameClasseStudent = (SchoolLevels)System.Enum.Parse(typeof(SchoolLevels), this.gameObject.name);
                ActionButtonDown();
            }
        }
        //bouton déjà selectionné
        else
        {
            DataGame.Instance.DataPlayer.NameClasseStudent = SchoolLevels.None;
            ActionButtonUp();
        }
    }

/*    /// <summary>
    /// Tmp before VR
    /// </summary>
    private void OnMouseUp()
    {
        ButtonUp();
    }*/

    private void ActionButtonUp()
    {
        ButtonUp();
        _buttonValidateConfiguration.UpdateButtonClasseUp();
    }
    
    private void ActionButtonDown()
    {
        ButtonDown();
        _buttonValidateConfiguration.UpdateButtonClasseDown();
    }

    public void ButtonUp()
    {
        _isButtonDown = false;
        _rendererComponent.material.color = COLOR_DEFAULT;
        this.gameObject.transform.position = this.gameObject.transform.position + _movePositionButton;
    }

    public void ButtonDown()
    {
        _isButtonDown = true;
        _rendererComponent.material.color = COLOR_ACTIVE;
        this.gameObject.transform.position = this.gameObject.transform.position - _movePositionButton;
    }

    private void OnTriggerEnter(Collider other)
    {
        OVRGrabber go = other.gameObject.GetComponent<OVRGrabber>() ??
                        other.gameObject.GetComponentInParent<OVRGrabber>() ??
                        other.gameObject.transform.parent.GetComponentInParent<OVRGrabber>();
        if (go != null)
        {
            //Debug.Log("CLICK CLASS");
            this.OnMouseDown();
        }
    }



    private void AddTextButtonClasse(string textCube)
    {
        GameObject tmpGameObjectText = new GameObject();
        
        tmpGameObjectText.transform.SetParent(this.gameObject.transform);
        tmpGameObjectText.gameObject.name = this.gameObject.name + "Text";
        
        TextMesh textMesh = tmpGameObjectText.AddComponent<TextMesh>();
        textMesh.text = textCube;
        textMesh.color = Color.black;
        textMesh.anchor = TextAnchor.MiddleCenter;
        textMesh.alignment = TextAlignment.Center;
        textMesh.fontSize = 50;
        textMesh.transform.localScale = new Vector3(0.05f, 0.1f, 1f);
        textMesh.transform.Rotate(90f, 0f, 0f);
        textMesh.transform.localPosition = new Vector3(0f, 0.5f, 0f);
    }
}
