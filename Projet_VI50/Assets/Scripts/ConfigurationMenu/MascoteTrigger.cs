﻿using System.Collections.Generic;
using UnityEngine;

public class MascoteTrigger : MonoBehaviour
{

	public List<GameObject> GameObjectsInTrigger;

	private void OnTriggerEnter(Collider other)
	{
		MascotGrabbable mascotGrabbable = other.gameObject.GetComponent<MascotGrabbable>();
		if (mascotGrabbable != null) 
			GameObjectsInTrigger.Add(other.gameObject);
	}

	private void OnTriggerExit(Collider other)
	{
		MascotGrabbable mascotGrabbable = other.gameObject.GetComponent<MascotGrabbable>();
		if (mascotGrabbable != null) 
			GameObjectsInTrigger.Remove(other.gameObject);
	}
}
