﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class BoxMascotConfiguration : MonoBehaviour
{
    [SerializeField] private ButtonValidateConfiguration _buttonValidateConfiguration;
    
    public static readonly Color COLOR_ACTIVE = new Color(0.05f, 1f, 0.13f);
    public static readonly Color COLOR_DEFAULT = new Color(0.1f, 0.93f, 1f);
    
    private Renderer[] _renderComponentChildren;

    private bool _isMascotInside = false;

    public List<Collider> TriggerList;

    public bool IsMascotInside
    {
        get { return _isMascotInside; }
    }

    private void Start()
    {
        TriggerList = new List<Collider>();
        _renderComponentChildren = GetComponentsInChildren<Renderer>();
        foreach (var renderer in _renderComponentChildren)
        {
            renderer.material.color = COLOR_DEFAULT;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        MascotGrabbable tmpScript = other.gameObject.GetComponent<MascotGrabbable>();

        if (tmpScript != null)
        {
            if (TriggerList.Count == 0)
            {
                _isMascotInside = true;
                //mascote update selected gamemanager
                DataGame.Instance.DataPlayer.NameMascotSelected = other.gameObject.name;
                foreach (var renderer in _renderComponentChildren)
                {
                    renderer.material.color = COLOR_ACTIVE;
                }

                _buttonValidateConfiguration.UpdateBoxMascoteIn();
                //if the object is not already in the list
                if(!TriggerList.Contains(other))
                {
                    //add the object to the list
                    TriggerList.Add(other);
                }
            } else if (TriggerList.Count >= 1)
            {
                //if the object is not already in the list
                if(!TriggerList.Contains(other))
                {
                    //add the object to the list
                    TriggerList.Add(other);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        MascotGrabbable tmpScript = other.gameObject.GetComponent<MascotGrabbable>();

        if (tmpScript != null)
        {
            if (TriggerList.Count == 1)
            {
                //mascote update selected gamemanager
                DataGame.Instance.DataPlayer.NameMascotSelected = String.Empty;
                foreach (var renderer in _renderComponentChildren)
                {
                    renderer.material.color = COLOR_DEFAULT;
                }
                _buttonValidateConfiguration.UpdateBoxMascoteOut();
                //if the object is in the list
                if(TriggerList.Contains(other))
                {
                    //remove it from the list
                    TriggerList.Remove(other);
                }
            } else if (TriggerList.Count >= 2)
            {
                _isMascotInside = false;
                //if the object is in the list
                if(TriggerList.Contains(other))
                {
                    //remove it from the list
                    TriggerList.Remove(other);
                }
                DataGame.Instance.DataPlayer.NameMascotSelected = TriggerList[0].name;
            }
        }
    }
}
