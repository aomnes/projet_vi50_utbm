using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMascot : MonoBehaviour {

    [SerializeField] private MascotScriptableObject[] _mascotes;
    [SerializeField] private Vector3 _offSet;

    private void Start()
    {
        InitializeMascots();
    }

    private void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
    }

    private void InitializeMascots()
    {
        float angleRotation = 0f;
        int numberMascot = _mascotes.Length;


        for (int numMascot = 0; numMascot < numberMascot; numMascot++)
        {
            GameObject tmpPrefab = Instantiate(_mascotes[numMascot].mascotPrefab);
            tmpPrefab.transform.localPosition = tmpPrefab.transform.localPosition + _offSet;

            //Temporary Empty GameObject in order to use it as center when rotating tmpPrefab
            GameObject tmpCenterRotation = new GameObject("TmpCenterRotation");
            tmpPrefab.transform.SetParent(tmpCenterRotation.transform, true);
            
            //change name instance
            tmpPrefab.name = _mascotes[numMascot].name;
            angleRotation = numMascot * 360.0f / numberMascot;
            tmpCenterRotation.transform.rotation = Quaternion.Euler(0f, angleRotation, 0f);
            tmpCenterRotation.transform.position = this.transform.position;
            tmpPrefab.transform.SetParent(this.transform);
            Destroy(tmpCenterRotation);
        }
    }
}
