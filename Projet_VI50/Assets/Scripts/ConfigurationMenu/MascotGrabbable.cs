﻿using UnityEngine;
using Debug = UnityEngine.Debug;

public class MascotGrabbable : OVRGrabbable
{
    [Header("My Variables")]
    [SerializeField] private MascoteTrigger _mascoteTriggerScript;

    private Rigidbody rb;

    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        _mascoteTriggerScript = GetComponentInParent<MascoteTrigger>();
    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        //la mascotte est depose dans la zone du porte mascotte
        if (_mascoteTriggerScript.GameObjectsInTrigger.Contains(this.gameObject))
        {
            rb.isKinematic = true;
            //on veut pas lui donner de vitesse ni de rotation
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
        //la mascotte N'est PAS depose dans la zone trigger du porte mascotte
        else
        {
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.velocity = linearVelocity;
            rb.angularVelocity = angularVelocity;
        }
        m_grabbedBy = null;
        m_grabbedCollider = null;
    }
}
