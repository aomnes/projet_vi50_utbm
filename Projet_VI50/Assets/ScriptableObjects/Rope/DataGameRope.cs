﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "DataGame", menuName = "RopeGameData")]
public class DataGameRope : ScriptableObject
{
    public FormWordData[] FormWordDataListCP;
    public FormWordData[] FormWordDataListCE1;
    public FormWordData[] FormWordDataListCE2;
    public FormWordData[] FormWordDataListCM1;
    public FormWordData[] FormWordDataListCM2;

    public SpriteData[] SpriteListCP;
    public SpriteData[] SpriteListCE1;
    public SpriteData[] SpriteListCE2;
    public SpriteData[] SpriteListCM1;
    public SpriteData[] SpriteListCM2;

    public MathData[] MathListCP;
    public MathData[] MathListCE1;
    public MathData[] MathListCE2;
    public MathData[] MathListCM1;
    public MathData[] MathListCM2;
}

[Serializable]
public struct MathData
{
    public string Operation;
    public string Result;
};

[Serializable]
public struct SpriteData
{
    public string name;
    public Sprite Image;
}

[Serializable]
public struct FormWordData
{
    public string Word;
    public Sprite Image;
};
