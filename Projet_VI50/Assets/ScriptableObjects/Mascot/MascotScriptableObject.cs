﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Mascot", fileName = "New Mascot")]
public class MascotScriptableObject : ScriptableObject {

	public new string name;
	public string themeMascotName;
	public Material themeMascotSkybox;
	public Material themeMascotGround;

	public GameObject mascotPrefab;
	public Sprite spriteHeadMascot;
	public Sprite spriteMascot;
}
