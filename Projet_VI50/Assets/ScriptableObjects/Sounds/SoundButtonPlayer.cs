﻿using UnityEngine;

[CreateAssetMenu(menuName = "Audio Events/Button")]
public class SoundButtonPlayer : AudioEvent {

	public AudioClip[] clips;

	[Range(0f, 1f)]
	public float volume;

	public override void Play(AudioSource source) {
		if (clips.Length == 0)
			return;
		source.clip = clips[Random.Range(0, clips.Length)];
		source.volume = this.volume;
		source.Play();
	}
}
