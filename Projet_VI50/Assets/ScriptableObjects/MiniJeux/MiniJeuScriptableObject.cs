﻿using UnityEngine;

[CreateAssetMenu(menuName ="Create Game", fileName = "New Game")]
public class MiniJeuScriptableObject : ScriptableObject
{
    public NameScene NameScene;
    public GameType GameType;
    public int Score;
    public int TotalTime;
    public Material ImageGameMaterial;
    
    public bool IsDone;
}
